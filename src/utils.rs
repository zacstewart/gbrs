pub trait SplitBytes {
    fn split(&self) -> (u8, u8);
}

impl SplitBytes for u16 {
    fn split(&self) -> (u8, u8) {
        let upper = (self >> 8) as u8;
        let lower = (self & 0x00ff) as u8;
        (upper, lower)
    }
}

pub trait JoinBytes {
    fn join(&self) -> u16;
}

impl JoinBytes for (u8, u8) {
    fn join(&self) -> u16 {
        let upper = (self.0 as u16) << 8;
        let lower = self.1 as u16;
        upper | lower
    }
}

pub trait CheckBit {
    fn bit(&self, bit: u8) -> bool;
}

impl CheckBit for u16 {
    fn bit(&self, bit: u8) -> bool {
        let mask = 1 << bit;
        self & mask == mask
    }
}

impl CheckBit for u8 {
    fn bit(&self, bit: u8) -> bool {
        let mask = 1 << bit;
        self & mask == mask
    }
}

#[cfg(test)]
mod test {
    use crate::utils::{JoinBytes, SplitBytes, CheckBit};

    #[test]
    fn split() {
        let(upper, lower) = 0x1234u16.split();
        assert_eq!(upper, 0x12);
        assert_eq!(lower, 0x34);
    }

    #[test]
    fn join() {
        let value = (0xbeu8, 0xefu8).join();
        assert_eq!(value, 0xbeef);
    }

    #[test]
    fn bit_u16() {
        let number: u16 = 0b1010_1010_1010_1010;
        assert!(number.bit(15));
        assert!(!number.bit(14));
        assert!(number.bit(13));
        assert!(!number.bit(12));
        assert!(number.bit(11));
        assert!(!number.bit(10));
        assert!(number.bit(9));
        assert!(!number.bit(8));
        assert!(number.bit(7));
        assert!(!number.bit(6));
        assert!(number.bit(5));
        assert!(!number.bit(4));
        assert!(number.bit(3));
        assert!(!number.bit(2));
        assert!(number.bit(1));
        assert!(!number.bit(0));
    }

    #[test]
    fn bit_u8() {
        let number: u8 = 0b1010_1010;
        assert!(number.bit(7));
        assert!(!number.bit(6));
        assert!(number.bit(5));
        assert!(!number.bit(4));
        assert!(number.bit(3));
        assert!(!number.bit(2));
        assert!(number.bit(1));
        assert!(!number.bit(0));
    }
}
