use std::fs::File;
use std::io::prelude::*;
use std::io;

use crate::mmu::ReadByte;

#[derive(Debug)]
pub struct Cartridge {
    rom: Box<[u8]>,
    ram: Box<[u8]>,
    ram_enabled: bool
}

impl Cartridge {
    pub fn new(rom: Box<[u8]>) -> Cartridge {
        Cartridge {
            rom: rom,
            ram: Box::new([0; 0x1fff]),
            ram_enabled: false
        }
    }

    pub fn from_file(filename: &str) -> Result<Cartridge, io::Error> {
        let mut data = Vec::new();
        let _len = File::open(filename)?.read_to_end(&mut data)?;
        Ok(Cartridge::new(data.into_boxed_slice()))
    }

    pub fn size(&self) -> usize {
        self.rom.len()
    }
}

impl ReadByte for Cartridge {
    fn read_byte(&self, address: u16) -> u8 {
        match address {
            // 0x0000..0x4000 (exclusive)
            0x0000..=0x3fff => *self.rom.get(address as usize).unwrap_or(&0),
            _ => {
                panic!("Reading outside of Cartridge's valid address space: {:04x}", address)
            }
        }
    }
}

#[test]
fn cartridge_new() {
    let cart = Cartridge::new(Box::new([0, 0, 0, 0]));
    assert_eq!(cart.size(), 4);
}

#[test]
fn cartridge_from_file() {
    match Cartridge::from_file("data/Tetris.gb") {
        Err(err) => assert!(false, "Failed to load rom: {}", err),
        Ok(cart) => {
            assert_eq!(cart.size(), 32768);
        }
    }
}

#[test]
fn read_byte() {
    let cart = Cartridge::new(Box::new([4, 3, 2, 1]));
    assert_eq!(cart.read_byte(0), 4);
    assert_eq!(cart.read_byte(1), 3);
    assert_eq!(cart.read_byte(2), 2);
    assert_eq!(cart.read_byte(3), 1);
}
