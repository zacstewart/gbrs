use crate::cartridge::Cartridge;
use crate::cpu::interrupts::Interrupts;

pub trait ReadByte {
    fn read_byte(&self, address: u16) -> u8;
}

pub trait WriteByte {
    fn write_byte(&mut self, address: u16, value: u8);
}

#[derive(Debug)]
pub struct MMU {
    cartridge: Cartridge,
    pub interrupts: Interrupts,
    ram: [u8; 0x2000], // 8KiB ram
    hram: [u8; 127],
}

impl MMU {
    pub fn new(cartridge: Cartridge) -> MMU {
        MMU {
            cartridge: cartridge,
            interrupts: Interrupts::new(),
            ram: [0; 0x2000],
            hram: [0; 127],
        }
    }
}

impl ReadByte for MMU {
    fn read_byte(&self, address: u16) -> u8 {
        let value = match address {
            // 0x0000..0x8000 Cartridge
            0x0000..=0x7fff => self.cartridge.read_byte(address), // [Cartridge] ROM bank 0 & switchable ROM bank
            // 0xc000..0xe000
            0xc000..=0xdfff => self.ram[(address - 0xc000) as usize],
            0xff0f => self.interrupts.if_,
            0xffff => self.interrupts.ie,
            0xff80..=0xfffe => self.hram[(address - 0xff80) as usize],
            0x8000..=u16::MAX => panic!("Tried to read undefined memory address space: {:04x}", address)
        };
        debug!("mmu[{:04x}] -> {:02x}", address, value);
        value
    }
}

impl WriteByte for MMU {
    fn write_byte(&mut self, address: u16, value: u8) {
        debug!("mmu[{:04x}]={:02x}", address, value);
        match address {
            // 0xc000..0xe000
            0xc000..=0xdfff => self.ram[(address - 0xc000) as usize] = value,
            0xff0f => self.interrupts.if_ = value,
            0xffff => self.interrupts.ie = value,
            0xff80..=0xfffe => self.hram[(address - 0xff80) as usize] = value,
            _ => panic!("Tried to write undefined memory address space: {:04x}={:04x}", address, value)
        }
    }
}

#[cfg(test)]
mod test {
    use crate::cartridge::Cartridge;
    use crate::mmu::{ReadByte, WriteByte, MMU};

    #[test]
    fn read_ram() {
        let cart = Cartridge::new(Box::new([]));
        let mut mmu = MMU::new(cart);
        mmu.ram[0] = 0xaa;
        let value = mmu.read_byte(0xc000);
        assert_eq!(value, 0xaa);
    }

    #[test]
    fn write_ram() {
        let cart = Cartridge::new(Box::new([]));
        let mut mmu = MMU::new(cart);
        mmu.write_byte(0xc000, 0xaa);
        assert_eq!(mmu.ram[0], 0xaa);
    }

    #[test]
    fn read_hram() {
        let cart = Cartridge::new(Box::new([]));
        let mut mmu = MMU::new(cart);
        mmu.hram[0] = 0xaa;
        mmu.hram[126] = 0xab;
        assert_eq!(mmu.read_byte(0xff80), 0xaa);
        assert_eq!(mmu.read_byte(0xfffe), 0xab);
    }

    #[test]
    fn write_hram() {
        let cart = Cartridge::new(Box::new([]));
        let mut mmu = MMU::new(cart);
        mmu.write_byte(0xff80, 0xaa);
        mmu.write_byte(0xfffe, 0xab);
        assert_eq!(mmu.hram[0], 0xaa);
        assert_eq!(mmu.hram[126], 0xab);
    }
}
