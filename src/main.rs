use std::env;

#[macro_use]
extern crate log;

mod cartridge;
mod cpu;
mod mmu;
mod utils;

fn main() {
    env_logger::init();
    let args: Vec<_> = env::args().collect();
    let cart = cartridge::Cartridge::from_file(&args[1]).unwrap();
    let mmu = mmu::MMU::new(cart);
    let mut cpu = cpu::CPU::new(mmu);

    loop {
        cpu.step();
        cpu.timer.burn_cycles();
    }
}
