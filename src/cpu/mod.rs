mod addressing;
pub mod interrupts;
#[cfg(test)]
mod test;

use crate::mmu::ReadByte;
use crate::mmu::MMU;
use crate::cpu::addressing::{
    ReadByteAddressingMode,
    ReadSignedByteAddressingMode,
    ReadWordAddressingMode,
    WriteByteAddressingMode,
    WriteWordAddressingMode
};
use crate::utils::{CheckBit, JoinBytes, SplitBytes};

macro_rules! decode_op {
    ($this:ident, $op:expr) => {
        match $op {
            0x00 => $this.nop(),
            0x01 => $this.ld_u16(addressing::RegisterWord(U16Register::BC), addressing::ImmediateWord),
            0x02 => $this.ld_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::BC)), addressing::RegisterByte(U8Register::A)),
            0x03 => $this.inc_r16(addressing::RegisterWord(U16Register::BC)),
            0x04 => $this.inc_u8(addressing::RegisterByte(U8Register::B)),
            0x05 => $this.dec_u8(addressing::RegisterByte(U8Register::B)),
            0x07 => $this.rlca(),
            0x08 => $this.ld_u16(addressing::MemoryAtU16Address(addressing::ImmediateWord), addressing::RegisterWord(U16Register::SP)),
            0x09 => $this.add_r16(addressing::RegisterWord(U16Register::HL), addressing::RegisterWord(U16Register::BC)),
            0x0a => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::BC))),
            0x0b => $this.dec_r16(addressing::RegisterWord(U16Register::BC)),
            0x0c => $this.inc_u8(addressing::RegisterByte(U8Register::C)),
            0x0d => $this.dec_u8(addressing::RegisterByte(U8Register::C)),
            0x0f => $this.rrca(),
            0x12 => $this.ld_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::DE)), addressing::RegisterByte(U8Register::A)),
            0x13 => $this.inc_r16(addressing::RegisterWord(U16Register::DE)),
            0x14 => $this.inc_u8(addressing::RegisterByte(U8Register::D)),
            0x15 => $this.dec_u8(addressing::RegisterByte(U8Register::D)),
            0x17 => $this.rla(),
            0x18 => $this.jr(Condition::Unconditional),
            0x19 => $this.add_r16(addressing::RegisterWord(U16Register::HL), addressing::RegisterWord(U16Register::DE)),
            0x1a => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::DE))),
            0x1b => $this.dec_r16(addressing::RegisterWord(U16Register::DE)),
            0x1c => $this.inc_u8(addressing::RegisterByte(U8Register::E)),
            0x1d => $this.dec_u8(addressing::RegisterByte(U8Register::E)),
            0x1f => $this.rra(),
            0x20 => $this.jr(Condition::NZ),
            0x22 => $this.ld_hli_a(),
            0x23 => $this.inc_r16(addressing::RegisterWord(U16Register::HL)),
            0x24 => $this.inc_u8(addressing::RegisterByte(U8Register::H)),
            0x25 => $this.dec_u8(addressing::RegisterByte(U8Register::H)),
            0x27 => $this.daa(),
            0x28 => $this.jr(Condition::Z),
            0x29 => $this.add_r16(addressing::RegisterWord(U16Register::HL), addressing::RegisterWord(U16Register::HL)),
            0x2a => $this.ld_a_hli(),
            0x2b => $this.dec_r16(addressing::RegisterWord(U16Register::HL)),
            0x2c => $this.inc_u8(addressing::RegisterByte(U8Register::L)),
            0x2d => $this.dec_u8(addressing::RegisterByte(U8Register::L)),
            0x2f => $this.cpl(),
            0x30 => $this.jr(Condition::NC),
            0x32 => $this.ld_hld_a(),
            0x33 => $this.inc_r16(addressing::RegisterWord(U16Register::SP)),
            0x34 => $this.inc_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x35 => $this.dec_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x37 => $this.scf(),
            0x38 => $this.jr(Condition::C),
            0x39 => $this.add_r16(addressing::RegisterWord(U16Register::HL), addressing::RegisterWord(U16Register::SP)),
            0x3a => $this.ld_a_hld(),
            0x3b => $this.dec_r16(addressing::RegisterWord(U16Register::SP)),
            0x3c => $this.inc_u8(addressing::RegisterByte(U8Register::A)),
            0x3d => $this.dec_u8(addressing::RegisterByte(U8Register::A)),
            0x3f => $this.ccf(),
            0x40 => $this.ld_u8(addressing::RegisterByte(U8Register::B), addressing::RegisterByte(U8Register::B)),
            0x41 => $this.ld_u8(addressing::RegisterByte(U8Register::B), addressing::RegisterByte(U8Register::C)),
            0x42 => $this.ld_u8(addressing::RegisterByte(U8Register::B), addressing::RegisterByte(U8Register::D)),
            0x43 => $this.ld_u8(addressing::RegisterByte(U8Register::B), addressing::RegisterByte(U8Register::E)),
            0x44 => $this.ld_u8(addressing::RegisterByte(U8Register::B), addressing::RegisterByte(U8Register::H)),
            0x45 => $this.ld_u8(addressing::RegisterByte(U8Register::B), addressing::RegisterByte(U8Register::L)),
            0x46 => $this.ld_u8(addressing::RegisterByte(U8Register::B), addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x4e => $this.ld_u8(addressing::RegisterByte(U8Register::C), addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x56 => $this.ld_u8(addressing::RegisterByte(U8Register::D), addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x5e => $this.ld_u8(addressing::RegisterByte(U8Register::E), addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x66 => $this.ld_u8(addressing::RegisterByte(U8Register::H), addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x6e => $this.ld_u8(addressing::RegisterByte(U8Register::L), addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x7e => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x47 => $this.ld_u8(addressing::RegisterByte(U8Register::B), addressing::RegisterByte(U8Register::A)),
            0x48 => $this.ld_u8(addressing::RegisterByte(U8Register::C), addressing::RegisterByte(U8Register::B)),
            0x49 => $this.ld_u8(addressing::RegisterByte(U8Register::C), addressing::RegisterByte(U8Register::C)),
            0x4a => $this.ld_u8(addressing::RegisterByte(U8Register::C), addressing::RegisterByte(U8Register::D)),
            0x4b => $this.ld_u8(addressing::RegisterByte(U8Register::C), addressing::RegisterByte(U8Register::E)),
            0x4c => $this.ld_u8(addressing::RegisterByte(U8Register::C), addressing::RegisterByte(U8Register::H)),
            0x4d => $this.ld_u8(addressing::RegisterByte(U8Register::C), addressing::RegisterByte(U8Register::L)),
            0x4f => $this.ld_u8(addressing::RegisterByte(U8Register::C), addressing::RegisterByte(U8Register::A)),
            0x50 => $this.ld_u8(addressing::RegisterByte(U8Register::D), addressing::RegisterByte(U8Register::B)),
            0x51 => $this.ld_u8(addressing::RegisterByte(U8Register::D), addressing::RegisterByte(U8Register::C)),
            0x52 => $this.ld_u8(addressing::RegisterByte(U8Register::D), addressing::RegisterByte(U8Register::D)),
            0x53 => $this.ld_u8(addressing::RegisterByte(U8Register::D), addressing::RegisterByte(U8Register::E)),
            0x54 => $this.ld_u8(addressing::RegisterByte(U8Register::D), addressing::RegisterByte(U8Register::H)),
            0x55 => $this.ld_u8(addressing::RegisterByte(U8Register::D), addressing::RegisterByte(U8Register::L)),
            0x57 => $this.ld_u8(addressing::RegisterByte(U8Register::D), addressing::RegisterByte(U8Register::A)),
            0x58 => $this.ld_u8(addressing::RegisterByte(U8Register::E), addressing::RegisterByte(U8Register::B)),
            0x59 => $this.ld_u8(addressing::RegisterByte(U8Register::E), addressing::RegisterByte(U8Register::C)),
            0x5a => $this.ld_u8(addressing::RegisterByte(U8Register::E), addressing::RegisterByte(U8Register::D)),
            0x5b => $this.ld_u8(addressing::RegisterByte(U8Register::E), addressing::RegisterByte(U8Register::E)),
            0x5c => $this.ld_u8(addressing::RegisterByte(U8Register::E), addressing::RegisterByte(U8Register::H)),
            0x5d => $this.ld_u8(addressing::RegisterByte(U8Register::E), addressing::RegisterByte(U8Register::L)),
            0x5f => $this.ld_u8(addressing::RegisterByte(U8Register::E), addressing::RegisterByte(U8Register::A)),
            0x60 => $this.ld_u8(addressing::RegisterByte(U8Register::H), addressing::RegisterByte(U8Register::B)),
            0x61 => $this.ld_u8(addressing::RegisterByte(U8Register::H), addressing::RegisterByte(U8Register::C)),
            0x62 => $this.ld_u8(addressing::RegisterByte(U8Register::H), addressing::RegisterByte(U8Register::D)),
            0x63 => $this.ld_u8(addressing::RegisterByte(U8Register::H), addressing::RegisterByte(U8Register::E)),
            0x64 => $this.ld_u8(addressing::RegisterByte(U8Register::H), addressing::RegisterByte(U8Register::H)),
            0x65 => $this.ld_u8(addressing::RegisterByte(U8Register::H), addressing::RegisterByte(U8Register::L)),
            0x67 => $this.ld_u8(addressing::RegisterByte(U8Register::H), addressing::RegisterByte(U8Register::A)),
            0x68 => $this.ld_u8(addressing::RegisterByte(U8Register::L), addressing::RegisterByte(U8Register::B)),
            0x69 => $this.ld_u8(addressing::RegisterByte(U8Register::L), addressing::RegisterByte(U8Register::C)),
            0x6a => $this.ld_u8(addressing::RegisterByte(U8Register::L), addressing::RegisterByte(U8Register::D)),
            0x6b => $this.ld_u8(addressing::RegisterByte(U8Register::L), addressing::RegisterByte(U8Register::E)),
            0x6c => $this.ld_u8(addressing::RegisterByte(U8Register::L), addressing::RegisterByte(U8Register::H)),
            0x6d => $this.ld_u8(addressing::RegisterByte(U8Register::L), addressing::RegisterByte(U8Register::L)),
            0x6f => $this.ld_u8(addressing::RegisterByte(U8Register::L), addressing::RegisterByte(U8Register::A)),
            0x70 => $this.ld_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL)), addressing::RegisterByte(U8Register::B)),
            0x71 => $this.ld_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL)), addressing::RegisterByte(U8Register::C)),
            0x72 => $this.ld_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL)), addressing::RegisterByte(U8Register::D)),
            0x73 => $this.ld_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL)), addressing::RegisterByte(U8Register::E)),
            0x74 => $this.ld_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL)), addressing::RegisterByte(U8Register::H)),
            0x75 => $this.ld_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL)), addressing::RegisterByte(U8Register::L)),
            0x77 => $this.ld_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL)), addressing::RegisterByte(U8Register::A)),
            0x78 => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::RegisterByte(U8Register::B)),
            0x79 => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::RegisterByte(U8Register::C)),
            0x7a => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::RegisterByte(U8Register::D)),
            0x7b => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::RegisterByte(U8Register::E)),
            0x7c => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::RegisterByte(U8Register::H)),
            0x7d => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::RegisterByte(U8Register::L)),
            0x7f => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::RegisterByte(U8Register::A)),
            0x80 => $this.add_a(addressing::RegisterByte(U8Register::B)),
            0x81 => $this.add_a(addressing::RegisterByte(U8Register::C)),
            0x82 => $this.add_a(addressing::RegisterByte(U8Register::D)),
            0x83 => $this.add_a(addressing::RegisterByte(U8Register::E)),
            0x84 => $this.add_a(addressing::RegisterByte(U8Register::H)),
            0x85 => $this.add_a(addressing::RegisterByte(U8Register::L)),
            0x86 => $this.add_a(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x87 => $this.add_a(addressing::RegisterByte(U8Register::A)),
            0x88 => $this.adc_a(addressing::RegisterByte(U8Register::B)),
            0x89 => $this.adc_a(addressing::RegisterByte(U8Register::C)),
            0x8a => $this.adc_a(addressing::RegisterByte(U8Register::D)),
            0x8b => $this.adc_a(addressing::RegisterByte(U8Register::E)),
            0x8c => $this.adc_a(addressing::RegisterByte(U8Register::H)),
            0x8d => $this.adc_a(addressing::RegisterByte(U8Register::L)),
            0x8e => $this.adc_a(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x8f => $this.adc_a(addressing::RegisterByte(U8Register::A)),
            0x90 => $this.sub_a(addressing::RegisterByte(U8Register::B)),
            0x91 => $this.sub_a(addressing::RegisterByte(U8Register::C)),
            0x92 => $this.sub_a(addressing::RegisterByte(U8Register::D)),
            0x93 => $this.sub_a(addressing::RegisterByte(U8Register::E)),
            0x94 => $this.sub_a(addressing::RegisterByte(U8Register::H)),
            0x95 => $this.sub_a(addressing::RegisterByte(U8Register::L)),
            0x96 => $this.sub_a(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x97 => $this.sub_a(addressing::RegisterByte(U8Register::A)),
            0x98 => $this.sbc_a(addressing::RegisterByte(U8Register::B)),
            0x99 => $this.sbc_a(addressing::RegisterByte(U8Register::C)),
            0x9a => $this.sbc_a(addressing::RegisterByte(U8Register::D)),
            0x9b => $this.sbc_a(addressing::RegisterByte(U8Register::E)),
            0x9c => $this.sbc_a(addressing::RegisterByte(U8Register::H)),
            0x9d => $this.sbc_a(addressing::RegisterByte(U8Register::L)),
            0x9e => $this.sbc_a(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0x9f => $this.sbc_a(addressing::RegisterByte(U8Register::A)),
            0x11 => $this.ld_u16(addressing::RegisterWord(U16Register::DE), addressing::ImmediateWord),
            0x21 => $this.ld_u16(addressing::RegisterWord(U16Register::HL), addressing::ImmediateWord),
            0x31 => $this.ld_u16(addressing::RegisterWord(U16Register::SP), addressing::ImmediateWord),
            0x06 => $this.ld_u8(addressing::RegisterByte(U8Register::B), addressing::ImmediateByte),
            0x0e => $this.ld_u8(addressing::RegisterByte(U8Register::C), addressing::ImmediateByte),
            0x16 => $this.ld_u8(addressing::RegisterByte(U8Register::D), addressing::ImmediateByte),
            0x1e => $this.ld_u8(addressing::RegisterByte(U8Register::E), addressing::ImmediateByte),
            0x26 => $this.ld_u8(addressing::RegisterByte(U8Register::H), addressing::ImmediateByte),
            0x2e => $this.ld_u8(addressing::RegisterByte(U8Register::L), addressing::ImmediateByte),
            0x3e => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::ImmediateByte),
            0xa0 => $this.and_a(addressing::RegisterByte(U8Register::B)),
            0xa1 => $this.and_a(addressing::RegisterByte(U8Register::C)),
            0xa2 => $this.and_a(addressing::RegisterByte(U8Register::D)),
            0xa3 => $this.and_a(addressing::RegisterByte(U8Register::E)),
            0xa4 => $this.and_a(addressing::RegisterByte(U8Register::H)),
            0xa5 => $this.and_a(addressing::RegisterByte(U8Register::L)),
            0xa6 => $this.and_a(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0xa7 => $this.and_a(addressing::RegisterByte(U8Register::A)),
            0xa8 => $this.xor_a(addressing::RegisterByte(U8Register::B)),
            0xa9 => $this.xor_a(addressing::RegisterByte(U8Register::C)),
            0xaa => $this.xor_a(addressing::RegisterByte(U8Register::D)),
            0xab => $this.xor_a(addressing::RegisterByte(U8Register::E)),
            0xac => $this.xor_a(addressing::RegisterByte(U8Register::H)),
            0xad => $this.xor_a(addressing::RegisterByte(U8Register::L)),
            0xae => $this.xor_a(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0xaf => $this.xor_a(addressing::RegisterByte(U8Register::A)),
            0xb0 => $this.or_a(addressing::RegisterByte(U8Register::B)),
            0xb1 => $this.or_a(addressing::RegisterByte(U8Register::C)),
            0xb2 => $this.or_a(addressing::RegisterByte(U8Register::D)),
            0xb3 => $this.or_a(addressing::RegisterByte(U8Register::E)),
            0xb4 => $this.or_a(addressing::RegisterByte(U8Register::H)),
            0xb5 => $this.or_a(addressing::RegisterByte(U8Register::L)),
            0xb6 => $this.or_a(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0xb7 => $this.or_a(addressing::RegisterByte(U8Register::A)),
            0xb8 => $this.cp_a(addressing::RegisterByte(U8Register::B)),
            0xb9 => $this.cp_a(addressing::RegisterByte(U8Register::C)),
            0xba => $this.cp_a(addressing::RegisterByte(U8Register::D)),
            0xbb => $this.cp_a(addressing::RegisterByte(U8Register::E)),
            0xbc => $this.cp_a(addressing::RegisterByte(U8Register::H)),
            0xbd => $this.cp_a(addressing::RegisterByte(U8Register::L)),
            0xbe => $this.cp_a(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL))),
            0xbf => $this.cp_a(addressing::RegisterByte(U8Register::A)),
            0xc0 => $this.ret(Condition::NZ),
            0xc1 => $this.pop(addressing::RegisterWord(U16Register::BC)),
            0xc2 => $this.jp(Condition::NZ, addressing::ImmediateWord),
            0xc3 => $this.jp(Condition::Unconditional, addressing::ImmediateWord),
            0xc4 => $this.call(Condition::NZ, addressing::ImmediateWord),
            0xc5 => $this.push(addressing::RegisterWord(U16Register::BC)),
            0xc6 => $this.add_a(addressing::ImmediateByte),
            0xc7 => $this.rst(ResetAddress::_00),
            0xc8 => $this.ret(Condition::Z),
            0xc9 => $this.ret(Condition::Unconditional),
            0xca => $this.jp(Condition::Z, addressing::ImmediateWord),
            0xcc => $this.call(Condition::Z, addressing::ImmediateWord),
            0xcd => $this.call(Condition::Unconditional, addressing::ImmediateWord),
            0xce => $this.adc_a(addressing::ImmediateByte),
            0xcf => $this.rst(ResetAddress::_08),
            0xd0 => $this.ret(Condition::NC),
            0xd1 => $this.pop(addressing::RegisterWord(U16Register::DE)),
            0xd2 => $this.jp(Condition::NC, addressing::ImmediateWord),
            0xd4 => $this.call(Condition::NC, addressing::ImmediateWord),
            0xd5 => $this.push(addressing::RegisterWord(U16Register::DE)),
            0xd6 => $this.sub_a(addressing::ImmediateByte),
            0xd7 => $this.rst(ResetAddress::_10),
            0xd8 => $this.ret(Condition::C),
            0xd9 => $this.reti(),
            0xda => $this.jp(Condition::C, addressing::ImmediateWord),
            0xdc => $this.call(Condition::C, addressing::ImmediateWord),
            0xde => $this.sbc_a(addressing::ImmediateByte),
            0xdf => $this.rst(ResetAddress::_18),
            0xe0 => $this.ld_u8(addressing::MemoryAtU8Address(addressing::ImmediateByte), addressing::RegisterByte(U8Register::A)),
            0xe1 => $this.pop(addressing::RegisterWord(U16Register::HL)),
            0xe2 => $this.ld_u8(addressing::MemoryAtU8Address(addressing::RegisterByte(U8Register::C)), addressing::RegisterByte(U8Register::A)),
            0xe5 => $this.push(addressing::RegisterWord(U16Register::HL)),
            0xe6 => $this.and_a(addressing::ImmediateByte),
            0xe7 => $this.rst(ResetAddress::_20),
            0xe8 => $this.add_sp_i8(),
            0xe9 => $this.jp_hl(),
            0xea => $this.ld_u8(addressing::MemoryAtU16Address(addressing::ImmediateWord), addressing::RegisterByte(U8Register::A)),
            0xee => $this.xor_a(addressing::ImmediateByte),
            0xef => $this.rst(ResetAddress::_28),
            0xf0 => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::MemoryAtU8Address(addressing::ImmediateByte)),
            0xf1 => $this.pop(addressing::RegisterWord(U16Register::AF)),
            0xf2 => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::MemoryAtU8Address(addressing::RegisterByte(U8Register::C))),
            0xf3 => $this.di(),
            0xf5 => $this.push(addressing::RegisterWord(U16Register::AF)),
            0xf6 => $this.or_a(addressing::ImmediateByte),
            0xf7 => $this.rst(ResetAddress::_30),
            0xf8 => $this.ld_hl_sp_i8(),
            0xf9 => $this.ld_u16(addressing::RegisterWord(U16Register::SP), addressing::RegisterWord(U16Register::HL)) + 4, // 4 cycles internal
            0xfa => $this.ld_u8(addressing::RegisterByte(U8Register::A), addressing::MemoryAtU16Address(addressing::ImmediateWord)),
            0xfb => $this.ei(),
            0xfe => $this.cp_a(addressing::ImmediateByte),
            0xff => $this.rst(ResetAddress::_38),
            _ => panic!("Unimplemented op: 0x{:02x}", $op)
        }
    }
}

pub struct Timer {
    accumulated_cycles: u8
}

impl Timer {
    fn new() -> Timer {
        Timer {
            accumulated_cycles: 0
        }
    }

    fn accumulate_cycles(&mut self, cycles: u8) {
        self.accumulated_cycles += cycles;
    }

    // FIXME: this should cost some real time to govern emulator execution speed
    pub fn burn_cycles(&mut self) {
        self.accumulated_cycles %= 4;
    }
}

pub enum U8Register {
    A, B, C, D, E, H, L, F
}

#[derive(Clone, Copy)]
pub enum U16Register {
    AF, BC, DE, HL, SP, PC
}

pub enum Condition {
    Unconditional,
    NZ,
    NC,
    Z,
    C
}

impl Condition {
    fn evaluate(&self, cpu: &CPU) -> bool {
        match self {
            Condition::Unconditional => true,
            Condition::NZ => !cpu.registers.flag_z(),
            Condition::NC => !cpu.registers.flag_c(),
            Condition::Z => cpu.registers.flag_z(),
            Condition::C => cpu.registers.flag_c()
        }
    }
}

enum ResetAddress {
    _00 = 0x0000,
    _08 = 0x0008,
    _10 = 0x0010,
    _18 = 0x0018,
    _20 = 0x0020,
    _28 = 0x0028,
    _30 = 0x0030,
    _38 = 0x0038
}

#[test]
fn test_condition() {
    use crate::cartridge::Cartridge;

    let cart = Cartridge::new(Box::new([0x00]));
    let mmu = MMU::new(cart);
    let mut cpu = CPU::new(mmu);

    assert!(Condition::Unconditional.evaluate(&cpu));
    assert!(Condition::NZ.evaluate(&cpu));
    assert!(Condition::NC.evaluate(&cpu));
    assert!(!Condition::Z.evaluate(&cpu));
    assert!(!Condition::C.evaluate(&cpu));

    cpu.registers.f = Registers::FLAG_Z;

    assert!(Condition::Unconditional.evaluate(&cpu));
    assert!(!Condition::NZ.evaluate(&cpu));
    assert!(Condition::NC.evaluate(&cpu));
    assert!(Condition::Z.evaluate(&cpu));
    assert!(!Condition::C.evaluate(&cpu));

    cpu.registers.f = Registers::FLAG_C;

    assert!(Condition::Unconditional.evaluate(&cpu));
    assert!(Condition::NZ.evaluate(&cpu));
    assert!(!Condition::NC.evaluate(&cpu));
    assert!(!Condition::Z.evaluate(&cpu));
    assert!(Condition::C.evaluate(&cpu));
}

struct Registers {
    a: u8, // Accumulator
    f: u8, // Flags
    b: u8,
    c: u8,
    d: u8,
    e: u8,
    h: u8,
    l: u8,
    sp: u16,
    pc: u16
}

impl Registers {
    const FLAG_Z: u8 = 0b1000_0000;
    const FLAG_N: u8 = 0b0100_0000;
    const FLAG_H: u8 = 0b0010_0000;
    const FLAG_C: u8 = 0b0001_0000;
    const FLAG_NONE: u8 = 0b0000_0000;

    fn new() -> Registers {
        Registers {
            a: 0,
            f: 0,
            b: 0,
            c: 0,
            d: 0,
            e: 0,
            h: 0,
            l: 0,
            sp: 0xfffe,
            pc: 0x0100,
        }
    }

    fn flag_z(&self) -> bool {
        self.f & Registers::FLAG_Z == Registers::FLAG_Z
    }

    fn flag_n(&self) -> bool {
        self.f & Registers::FLAG_N == Registers::FLAG_N
    }

    fn flag_h(&self) -> bool {
        self.f & Registers::FLAG_H == Registers::FLAG_H
    }

    fn flag_c(&self) -> bool {
        self.f & Registers::FLAG_C == Registers::FLAG_C
    }
}

#[test]
fn test_flags() {
    // Flags are stored in the upper 4 bytes of F in Z N H C order
    let mut registers = Registers::new();
    registers.f = Registers::FLAG_Z;
    assert!(registers.flag_z());
    assert!(!registers.flag_n());
    assert!(!registers.flag_h());
    assert!(!registers.flag_c());
    registers.f = Registers::FLAG_N;
    assert!(!registers.flag_z());
    assert!(registers.flag_n());
    assert!(!registers.flag_h());
    assert!(!registers.flag_c());
    registers.f = Registers::FLAG_H;
    assert!(!registers.flag_z());
    assert!(!registers.flag_n());
    assert!(registers.flag_h());
    assert!(!registers.flag_c());
    registers.f = Registers::FLAG_C;
    assert!(!registers.flag_z());
    assert!(!registers.flag_n());
    assert!(!registers.flag_h());
    assert!(registers.flag_c());
}

pub struct CPU {
    mmu: MMU,
    pub timer: Timer,
    registers: Registers,
}

impl CPU {
    pub fn new(mmu: MMU) -> CPU {
        CPU {
            mmu: mmu,
            timer: Timer::new(),
            registers: Registers::new(),
        }
    }

    pub fn step(&mut self) {
        self.mmu.interrupts.step();
        let (fetch_cycles, instruction) = addressing::ImmediateByte.load(self);
        let execute_cycles = decode_op!(self, instruction);
        self.timer.accumulate_cycles(fetch_cycles + execute_cycles);
    }

    // 8-bit loads

    fn ld_u8<WM, RM>(&mut self, dst: WM, src: RM) -> u8
    where
        WM: WriteByteAddressingMode,
        RM: ReadByteAddressingMode
    {
        let (load_cycles, value) = src.load(self);
        let store_cycles = dst.store(self, value);
        load_cycles + store_cycles
    }


    fn ld_hli_a(&mut self) -> u8 {
        self.ld_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL)), addressing::RegisterByte(U8Register::A));
        let inc_cycles = self.inc_r16(addressing::RegisterWord(U16Register::HL));
        inc_cycles
    }

    fn ld_hld_a(&mut self) -> u8 {
        self.ld_u8(addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL)), addressing::RegisterByte(U8Register::A));
        let dec_cycles = self.dec_r16(addressing::RegisterWord(U16Register::HL));
        dec_cycles
    }

    fn ld_a_hli(&mut self) -> u8 {
        self.ld_u8(addressing::RegisterByte(U8Register::A), addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL)));
        let inc_cycles = self.inc_r16(addressing::RegisterWord(U16Register::HL));
        inc_cycles
    }

    fn ld_a_hld(&mut self) -> u8 {
        self.ld_u8(addressing::RegisterByte(U8Register::A), addressing::MemoryAtU16Address(addressing::RegisterWord(U16Register::HL)));
        let dec_cycles = self.dec_r16(addressing::RegisterWord(U16Register::HL));
        dec_cycles
    }

    // 8-bit arithmetic

    fn inc_u8<AM>(&mut self, am: AM) -> u8
        where AM: ReadByteAddressingMode + WriteByteAddressingMode
    {
        let (load_cycles, value) = am.load(self);
        let (result, flags) = CPU::add_u8_u8(value, 1);
        let flags = flags & !Registers::FLAG_C;
        let store_cycles = am.store(self, result);
        let set_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, flags);
        load_cycles + store_cycles + set_flags_cycles
    }

    fn dec_u8<AM>(&mut self, am: AM) -> u8
        where AM: ReadByteAddressingMode + WriteByteAddressingMode
    {
        let (load_cycles, value) = am.load(self);
        let (result, flags) = CPU::sub_u8_u8(value, 1);
        let flags = flags & !Registers::FLAG_C;
        let store_cycles = am.store(self, result);
        let set_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, flags);
        load_cycles + store_cycles + set_flags_cycles
    }

    fn add_a<AM>(&mut self, am: AM) -> u8
        where AM: ReadByteAddressingMode
    {
        let a_am = addressing::RegisterByte(U8Register::A);
        let (load_a_cycles, a) = a_am.load(self);
        let (load_rhs_cycles, rhs) = am.load(self);
        let (result, flags) = CPU::add_u8_u8(a, rhs);
        let store_cycles = a_am.store(self, result);
        let store_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, flags);
        load_a_cycles + load_rhs_cycles + store_cycles + store_flags_cycles
    }

    fn sub_a<AM>(&mut self, am: AM) -> u8
        where AM: ReadByteAddressingMode
    {
        let a_am = addressing::RegisterByte(U8Register::A);
        let (load_a_cycles, a) = a_am.load(self);
        let (load_cycles, rhs) = am.load(self);
        let (result, flags) = CPU::sub_u8_u8(a, rhs);
        let store_cycles = a_am.store(self, result);
        let set_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, flags);
        load_a_cycles + load_cycles + store_cycles + set_flags_cycles
    }

    fn and_a<AM>(&mut self, am: AM) -> u8
        where AM: ReadByteAddressingMode
    {
        let a_am = addressing::RegisterByte(U8Register::A);
        let (load_a_cycles, a) = a_am.load(self);
        let (load_cycles, rhs) = am.load(self);
        let result = a & rhs;
        let store_cycles = a_am.store(self, result);
        let z = if result == 0 { Registers::FLAG_Z } else { Registers::FLAG_NONE };
        let flags = z | Registers::FLAG_H;
        let set_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, flags);
        load_a_cycles + load_cycles + store_cycles + set_flags_cycles
    }

    fn or_a<AM>(&mut self, am: AM) -> u8
        where AM: ReadByteAddressingMode
    {
        let a_am = addressing::RegisterByte(U8Register::A);
        let (load_a_cycles, a) = a_am.load(self);
        let (load_cycles, rhs) = am.load(self);
        let result = a | rhs;
        let z = if result == 0 { Registers::FLAG_Z } else { Registers::FLAG_NONE };
        let store_cycles = a_am.store(self, result);
        let set_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, z);
        load_a_cycles + load_cycles + store_cycles + set_flags_cycles
    }

    fn adc_a<AM>(&mut self, am: AM) -> u8
        where AM: ReadByteAddressingMode
    {
        let a_am = addressing::RegisterByte(U8Register::A);
        let (load_a_cycles, a) = a_am.load(self);
        let (load_cycles, rhs) = am.load(self);
        // FIXME: I'm still not 100% certain this if should be
        // 0b0000_000 or 0b0010_0000 (F & h-flag position)
        let cy = if self.registers.flag_c() { 1 } else { 0 };
        // FIXME: I'm also not 100% certain that carry and half-carry
        // should be considered while adding operand and cy
        let (result, flags1) = CPU::add_u8_u8(a, rhs);
        let (result, flags2) = CPU::add_u8_u8(result, cy);
        let flags = {
            let z = if result == 0 { Registers::FLAG_Z } else { Registers::FLAG_NONE };
            let hc = (Registers::FLAG_H | Registers::FLAG_C) & (flags1 | flags2);
            z | hc
        };
        let store_cycles = a_am.store(self, result);
        let set_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, flags);
        load_a_cycles + load_cycles + store_cycles + set_flags_cycles
    }

    fn sbc_a<AM>(&mut self, am: AM) -> u8
        where AM: ReadByteAddressingMode
    {
        let a_am = addressing::RegisterByte(U8Register::A);
        let (load_a_cycles, a) = a_am.load(self);
        let (load_cycles, operand) = am.load(self);
        let cy = if self.registers.flag_c() { 1 } else { 0 };

        let (result, flags1) = CPU::sub_u8_u8(a, operand);
        let (result, flags2) = CPU::sub_u8_u8(result, cy);
        let flags = {
            let hc = (Registers::FLAG_H | Registers::FLAG_C) & flags1;
            flags2 | Registers::FLAG_N | hc
        };
        let store_cycles = a_am.store(self, result);
        let set_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, flags);
        load_a_cycles + load_cycles + store_cycles + set_flags_cycles
    }

    fn xor_a<AM>(&mut self, operand_am: AM) -> u8
        where AM: ReadByteAddressingMode
    {
        let a_am = addressing::RegisterByte(U8Register::A);
        let (load_a_cycles, a) = a_am.load(self);
        let (load_operand_cycles, operand) = operand_am.load(self);
        let result = a ^ operand;
        let store_a_cycles = a_am.store(self, result);
        let flags = if result == 0 { Registers::FLAG_Z } else { Registers::FLAG_NONE };
        let store_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, flags);
        load_a_cycles + load_operand_cycles + store_a_cycles + store_flags_cycles
    }

    fn cp_a<AM>(&mut self, operand_am: AM) -> u8
        where AM: ReadByteAddressingMode
    {
        let a_am = addressing::RegisterByte(U8Register::A);
        let (load_a_cycles, a) = a_am.load(self);
        let (load_cycles, rhs) = operand_am.load(self);
        let (_result, flags) = CPU::sub_u8_u8(a, rhs);
        let set_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, flags);
        load_a_cycles + load_cycles + set_flags_cycles
    }

    fn daa(&mut self) -> u8 {
        let a_am = addressing::RegisterByte(U8Register::A);
        let flags_am = addressing::RegisterByte(U8Register::F);
        let (load_a_cycles, mut a) = a_am.load(self);
        let (load_flags_cycles, mut flags) = flags_am.load(self);

        if self.registers.flag_n() {
            if self.registers.flag_c() {
                a = a.wrapping_sub(0x60);
            }

            if self.registers.flag_h() {
                a = a.wrapping_sub(0x06);
            }
        } else {
            if self.registers.flag_c() || a > 0x99 {
                a = a.wrapping_add(0x60);
                flags |= Registers::FLAG_C;
            }
            if self.registers.flag_h() || (a & 0x0f) > 0x09 {
                a = a.wrapping_add(0x06);
            }
        }

        // Always reset h
        flags &= !Registers::FLAG_H;

        if a == 0 {
            flags |= Registers::FLAG_Z;
        } else {
            flags &= !Registers::FLAG_Z;
        }

        let store_a_cycles = a_am.store(self, a);
        let store_flags_cycles = flags_am.store(self, flags);

        load_a_cycles + load_flags_cycles + store_a_cycles + store_flags_cycles
    }

    fn scf(&mut self) -> u8 {
        let flags_am = addressing::RegisterByte(U8Register::F);
        let (load_cycles, mut flags) = flags_am.load(self);
        // keep current Z, unset rest
        flags &= Registers::FLAG_Z;
        // set C
        flags |= Registers::FLAG_C;
        let store_cycles = flags_am.store(self, flags);
        load_cycles + store_cycles
    }

    fn cpl(&mut self) -> u8 {
        let a_am = addressing::RegisterByte(U8Register::A);
        let flags_am = addressing::RegisterByte(U8Register::F);
        let (load_cycles, a) = a_am.load(self);
        let (load_flags_cycles, flags) = flags_am.load(self);
        let store_cycles = a_am.store(self, !a);
        let store_flags_cycles = flags_am.store(self, flags | Registers::FLAG_N | Registers::FLAG_H);
        load_cycles + load_flags_cycles + store_cycles + store_flags_cycles
    }

    fn ccf(&mut self) -> u8 {
        let flags_am = addressing::RegisterByte(U8Register::F);
        let (load_flags_cycles, mut flags) = flags_am.load(self);
        flags ^= Registers::FLAG_C;
        flags &= !(Registers::FLAG_N | Registers::FLAG_H);
        let store_flags_cycles = flags_am.store(self, flags);
        load_flags_cycles + store_flags_cycles
    }

    fn rlca(&mut self) -> u8 {
        let a_am = addressing::RegisterByte(U8Register::A);
        let (load_cycles, a) = a_am.load(self);
        let result = a.wrapping_shl(1) | a.wrapping_shr(7);
        let c = if a.bit(7) { Registers::FLAG_C } else { Registers::FLAG_NONE };
        let store_cycles = a_am.store(self, result);
        let store_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, c);
        load_cycles + store_cycles + store_flags_cycles

    }

    fn rla(&mut self) -> u8 {
        let a_am = addressing::RegisterByte(U8Register::A);
        let flags_am = addressing::RegisterByte(U8Register::F);
        let (load_cycles, a) = a_am.load(self);
        let (load_flags_cycles, flags) = flags_am.load(self);
        let c = if a.bit(7) { Registers::FLAG_C } else { Registers::FLAG_NONE };
        let result = a.wrapping_shl(1) | (flags & Registers::FLAG_C).wrapping_shr(4);
        let store_cycles = a_am.store(self, result);
        let store_flags_cycles = flags_am.store(self, c);
        load_cycles + load_flags_cycles + store_cycles + store_flags_cycles

    }

    fn rrca(&mut self) -> u8 {
        let a_am = addressing::RegisterByte(U8Register::A);
        let (load_cycles, a) = a_am.load(self);
        let result = a.wrapping_shr(1) | a.wrapping_shl(7);
        let c = if a.bit(0) { Registers::FLAG_C } else { Registers::FLAG_NONE };
        let store_cycles = a_am.store(self, result);
        let store_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, c);
        load_cycles + store_cycles + store_flags_cycles
    }

    fn rra(&mut self) -> u8 {
        let a_am = addressing::RegisterByte(U8Register::A);
        let flags_am = addressing::RegisterByte(U8Register::F);
        let (load_cycles, a) = a_am.load(self);
        let (load_flags_cycles, flags) = flags_am.load(self);
        let c = if a.bit(0) { Registers::FLAG_C } else { Registers::FLAG_NONE };
        let result = a.wrapping_shr(1) | (flags & Registers::FLAG_C).wrapping_shl(3);
        let store_cycles = a_am.store(self, result);
        let store_flags_cycles = flags_am.store(self, c);
        load_cycles + load_flags_cycles + store_cycles + store_flags_cycles
    }

    // 16-bit loads

    fn ld_u16<WM, RM>(&mut self, dst: WM, src: RM) -> u8
        where
            WM: WriteWordAddressingMode,
            RM: ReadWordAddressingMode
    {
        let (load_cycles, value) = src.load(self);
        let store_cycles = dst.store(self, value);
        load_cycles + store_cycles
    }

    fn ld_hl_sp_i8(&mut self) -> u8 {
        let (load_sp_cycles, sp) = addressing::RegisterWord(U16Register::SP).load(self);
        let (load_i8_cycles, immediate) = addressing::ImmediateSignedByte.load(self);
        let (result, flags) = self.add_i16_i16(sp as i16, immediate as i16);
        let store_cycles = addressing::RegisterWord(U16Register::HL).store(self, result as u16);
        let set_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, flags);
        load_sp_cycles + load_i8_cycles + store_cycles + set_flags_cycles + 4
    }

    // 16-bit arithmetic

    fn inc_r16(&mut self, am: addressing::RegisterWord) -> u8 {
        let (load_cycles, value) = am.load(self);
        let value = value.wrapping_add(1);
        let store_cycles = am.store(self, value);
        load_cycles + store_cycles + 4
    }

    fn dec_r16(&mut self, am: addressing::RegisterWord) -> u8 {
        let (load_cycles, value) = am.load(self);
        let value = value.wrapping_sub(1);
        let store_cycles = am.store(self, value);
        load_cycles + store_cycles + 4
    }

    fn add_r16<D, S>(&mut self, dst: D, src: S) -> u8
        where
            D: ReadWordAddressingMode + WriteWordAddressingMode,
            S: ReadWordAddressingMode
    {
        let (dst_load_cycles, lhs) = dst.load(self);
        let (src_load_cycles, rhs) = src.load(self);
        let result = lhs.wrapping_add(rhs);
        let store_cycles = dst.store(self, result);
        let check = lhs ^ rhs ^ result;
        let h = ((check & 0b0001_0000_0000_0000) >> 7) as u8;
        let c = if result < lhs { Registers::FLAG_C } else { Registers::FLAG_NONE };
        let flags = h | c;
        let set_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, flags);
        src_load_cycles + dst_load_cycles + store_cycles + set_flags_cycles + 4
    }

    fn add_sp_i8(&mut self) -> u8 {
        let sp_am = addressing::RegisterWord(U16Register::SP);
        let (load_sp_cycles, sp) = sp_am.load(self);
        let (load_immediate_cycles, immediate) = addressing::ImmediateSignedByte.load(self);
        let (result, flags) = self.add_i16_i16(sp as i16, immediate as i16);
        let store_sp_cycles = sp_am.store(self, result as u16);
        let set_flags_cycles = addressing::RegisterByte(U8Register::F).store(self, flags);
        load_sp_cycles + load_immediate_cycles + store_sp_cycles + set_flags_cycles + 8
    }

    fn push<AM: ReadWordAddressingMode>(&mut self, am: AM) -> u8 {
        let sp_am = addressing::RegisterWord(U16Register::SP);
        let mem_at_sp = addressing::MemoryAtU16Address(sp_am);
        let (load_sp_cycles, original_sp) = sp_am.load(self);

        let (load_cycles, value) = am.load(self);
        let (upper, lower) = value.split();

        let store_sp_upper_cycles = sp_am.store(self, original_sp - 1);
        let store_upper_cycles = WriteByteAddressingMode::store(&mem_at_sp, self, upper);
        let store_sp_lower_cycles = sp_am.store(self, original_sp - 2);
        let store_lower_cycles = WriteByteAddressingMode::store(&mem_at_sp, self, lower);

        load_cycles + load_sp_cycles + store_sp_upper_cycles + store_sp_lower_cycles + store_upper_cycles + store_lower_cycles + 4
    }

    fn pop<AM: WriteWordAddressingMode>(&mut self, am: AM) -> u8 {
        let sp_am = addressing::RegisterWord(U16Register::SP);
        let mem_at_sp = addressing::MemoryAtU16Address(sp_am);
        let (load_sp_cycles, original_sp) = sp_am.load(self);

        let (load_lower_cycles, lower) = mem_at_sp.load(self);
        let store_sp_lower_cycles = sp_am.store(self, original_sp + 1);
        let (load_upper_cycles, upper) = mem_at_sp.load(self);
        let store_sp_upper_cycles = sp_am.store(self, original_sp + 2);

        let value = (upper, lower).join();
        let store_cycles = am.store(self, value);

        load_sp_cycles + load_lower_cycles + load_upper_cycles + store_sp_lower_cycles + store_sp_upper_cycles + store_cycles
    }

    fn jr(&mut self, condition: Condition) -> u8 {
        let (load_immediate_cycles, immediate) = addressing::ImmediateSignedByte.load(self);
        let jump_cycles = if condition.evaluate(self) {
            let pc_am = addressing::RegisterWord(U16Register::PC);
            let (load_pc_cycles, pc) = pc_am.load(self);
            let (result, _flags) = self.add_i16_i16(pc as i16, immediate as i16);
            let store_pc_cycles = pc_am.store(self, result as u16);
            load_pc_cycles + store_pc_cycles + 4
        } else {
            0
        };

        load_immediate_cycles + jump_cycles
    }

    fn jp<AM>(&mut self, condition: Condition, am: AM) -> u8 where AM: ReadWordAddressingMode {
        let (load_cycles, address) = am.load(self);

        let jump_cycles = if condition.evaluate(self) {
            let cycles = addressing::RegisterWord(U16Register::PC).store(self, address);
            cycles + 4
        } else {
            0
        };

        load_cycles + jump_cycles
    }

    // This instruction is a one-off impl because the cycles cost (4) don't
    // add up nicely with the addressing modes used. I wasn't able to refactor
    // all the cycle arithmentic without massively screwing up stuff everywhere else.
    fn jp_hl(&mut self) -> u8 {
        self.jp(Condition::Unconditional, addressing::RegisterWord(U16Register::HL));
        0
    }

    fn rst(&mut self, address: ResetAddress) -> u8 {
        let pc = addressing::RegisterWord(U16Register::PC);
        let push_cycles = self.push(pc);
        let store_cycles = pc.store(self, address as u16);
        push_cycles + store_cycles
    }

    fn call<AM: ReadWordAddressingMode>(&mut self, condition: Condition, destination_am: AM) -> u8 {
        let pc = addressing::RegisterWord(U16Register::PC);
        let (load_cycles, destination) = destination_am.load(self);

        let call_cycles = if condition.evaluate(self) {
            let push_cycles = self.push(pc);
            let store_pc_cycles = pc.store(self, destination);
            push_cycles + store_pc_cycles
        } else {
            0
        };

        load_cycles + call_cycles
    }

    fn ret(&mut self, condition: Condition) -> u8 {
        let eval_cycles = match condition {
            Condition::Unconditional => 0,
            _ => 4
        };

        let pop_cycles = if condition.evaluate(self) {
            self.pop(addressing::RegisterWord(U16Register::PC)) + 4
        } else {
            0
        };

        pop_cycles + eval_cycles
    }

    fn reti(&mut self) -> u8 {
        let ret_cycles = self.ret(Condition::Unconditional);
        self.mmu.interrupts.immediately_enable();
        ret_cycles
    }

    // Misc

    fn nop(&self) -> u8 {
        0
    }

    fn di(&mut self) -> u8 {
        self.mmu.interrupts.disable();
        0
    }

    fn ei(&mut self) -> u8 {
        self.mmu.interrupts.enable();
        0
    }


    // Helpers

    fn add_u8_u8(lhs: u8, rhs: u8) -> (u8, u8) {
        let result = lhs.wrapping_add(rhs);
        let check = lhs ^ rhs ^ result;
        let z = if result == 0 { Registers::FLAG_Z } else { Registers::FLAG_NONE };
        let h = (check & 0b0001_0000) << 1;
        let c = if ((lhs as u16) + (rhs as u16)) > result as u16 {
            Registers::FLAG_C
        } else {
            Registers::FLAG_NONE
        };
        let flags = z | h | c;
        (result, flags)
    }

    fn sub_u8_u8(lhs: u8, rhs: u8) -> (u8, u8) {
        let result = lhs.wrapping_sub(rhs);
        let check = lhs ^ rhs ^ result;
        let z = if result == 0 { Registers::FLAG_Z } else { Registers::FLAG_NONE };
        let h = (check & 0b0001_0000) << 1;
        let n = Registers::FLAG_N;
        let c = if ((lhs as u16).wrapping_sub(rhs as u16)) > result as u16 {
            Registers::FLAG_C
        } else {
            Registers::FLAG_NONE
        };
        let flags = z | h | n | c;
        (result, flags)
    }

    fn add_i16_i16(&self, lhs: i16, rhs: i16) -> (i16, u8) {
        let result = lhs.wrapping_add(rhs);
        let check = lhs ^ rhs ^ result;
        let h = ((check & 0b0000_0000_0001_0000) << 1) as u8;
        let c = ((check & 0b0000_0001_0000_0000) >> 4) as u8;
        let flags = h | c;
        (result, flags)
    }

    pub fn take_byte(&mut self) -> u8 {
        let byte = self.mmu.read_byte(self.registers.pc);
        self.registers.pc += 1;
        byte
    }

    pub fn take_word(&mut self) -> u16 {
        let lower = self.take_byte();
        let upper = self.take_byte();
        (upper, lower).join()
    }
}
