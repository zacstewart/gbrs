#[derive(Debug)]
enum Switch {
    To(bool),
    Stay
}

#[derive(Debug)]
pub struct Interrupts {
    switch: Switch,
    pub ime: bool,
    pub if_: u8,
    pub ie: u8
}

impl Interrupts {
    pub fn new() -> Interrupts {
        Interrupts {
            switch: Switch::Stay,
            ime: false,
            if_: 0,
            ie: 0
        }
    }

    pub fn disable(&mut self) {
        self.switch = Switch::To(false);
    }

    pub fn enable(&mut self) {
        self.switch = Switch::To(true);
    }

    pub fn immediately_enable(&mut self) {
        self.ime = true;
        self.switch = Switch::Stay;
    }

    pub fn step(&mut self) {
        if let Switch::To(ime) = self.switch {
            self.ime = ime;
            self.switch = Switch::Stay;
        }
    }
}

