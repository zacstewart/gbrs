use crate::cartridge::Cartridge;
use crate::mmu::{ReadByte, WriteByte, MMU};
use crate::cpu::{CPU, Condition, U8Register, U16Register, Registers};
use crate::cpu::addressing::{ReadByteAddressingMode, ReadWordAddressingMode};
use crate::cpu::addressing;
use crate::utils::CheckBit;

#[macro_export]
macro_rules! synthesize_cart {
        ([$($program:expr),*]) => {{
            use crate::cartridge::Cartridge;
            let mut data = [0; 0x4000];
            // Begin code execution point in a cartridge is 0x0100
            for (i, &byte) in [$($program),*].iter().enumerate() {
                data[0x0100 + i] = byte;
            }
            Cartridge::new(Box::new(data))
        }}
    }

struct StaticByteAddressingMode(u8, u8);
struct StaticWordAddressingMode(u16, u8);

impl ReadByteAddressingMode for StaticByteAddressingMode {
    fn load(&self, _cpu: &mut CPU) -> (u8, u8) {
        (self.1, self.0)
    }
}

impl ReadWordAddressingMode for StaticWordAddressingMode {
    fn load(&self, _cpu: &mut CPU) -> (u8, u16) {
        (self.1, self.0)
    }
}

pub fn create_cpu(cart: Cartridge) -> CPU {
    let mmu = MMU::new(cart);
    CPU::new(mmu)
}

#[test]
fn nop() {
    let cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    assert_eq!(cpu.nop(), 0);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn nop_instruction() {
    let mut cpu = create_cpu(synthesize_cart!([0x00]));
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.f, flags_before);
}

// 8-bit loads

#[test]
fn ld_u8() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    let src = StaticByteAddressingMode(0x42, 4);
    let dst = addressing::RegisterByte(U8Register::B);
    let cycles = cpu.ld_u8(dst, src);
    assert_eq!(cycles, 4);
    assert_eq!(cpu.registers.b, 0x42);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_u8_register_with_immediate_byte() {
    let op_codes = [
        0x06, 0x0e,
        0x16, 0x1e,
        0x26, 0x2e,
        0x3e,
    ];
    for (i, &op_code) in op_codes.iter().enumerate() {
        let value = i as u8;
        let mut cpu = create_cpu(synthesize_cart!([op_code, value]));
        let flags_before = cpu.registers.f;
        cpu.step();
        match op_code {
            0x06 => assert_eq!(cpu.registers.b, value),
            0x0e => assert_eq!(cpu.registers.c, value),
            0x16 => assert_eq!(cpu.registers.d, value),
            0x1e => assert_eq!(cpu.registers.e, value),
            0x26 => assert_eq!(cpu.registers.h, value),
            0x2e => assert_eq!(cpu.registers.l, value),
            0x3e => assert_eq!(cpu.registers.a, value),
            _ => assert!(false, "Unexpected op op code {:02x}", op_code)
        }
        assert_eq!(cpu.timer.accumulated_cycles, 8);
        assert_eq!(cpu.registers.f, flags_before);
    }
}

#[test]
fn ld_memory_at_u16_register_with_u8_register() {
    let upper = 0xc0;
    let lower = 0xde;
    let op_codes = [0x02, 0x12, 0x70, 0x71, 0x72, 0x73, 0x77];
    for (i, &op_code) in op_codes.iter().enumerate() {
        let value = i as u8;
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        let flags_before = cpu.registers.f;

        match op_code {
            0x02 | 0x12 | 0x77 => cpu.registers.a = value,
            0x70 => cpu.registers.b = value,
            0x71 => cpu.registers.c = value,
            0x72 => cpu.registers.d = value,
            0x73 => cpu.registers.e = value,
            _ => assert!(false, "Unexpected op op code {:02x}", op_code)
        }

        match op_code {
            0x02 => {
                cpu.registers.b = upper;
                cpu.registers.c = lower;
            },
            0x12 => {
                cpu.registers.d = upper;
                cpu.registers.e = lower;
            }
            0x70..=0x77 => {
                cpu.registers.h = upper;
                cpu.registers.l = lower;
            }
            _ => assert!(false, "Unexpected op op code {:02x}", op_code)
        }

        cpu.step();
        let read_value = cpu.mmu.read_byte(0xc0de);
        assert_eq!(value, read_value, "Memory was not set correct for op code {:02x}", op_code);
        assert_eq!(cpu.timer.accumulated_cycles, 8);
        assert_eq!(cpu.registers.f, flags_before);
    }
}

#[test]
fn ld_u8_register_with_memory_at_u16_register() {
    let op_codes = [0x0a, 0x1a, 0x46, 0x4e, 0x56, 0x5e, 0x66, 0x6e, 0x7e];
    for &op_code in op_codes.iter() {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        let flags_before = cpu.registers.f;
        match op_code {
            0x0a => {
                cpu.registers.b = 0xc0;
                cpu.registers.c = 0xde;
            }
            0x1a => {
                cpu.registers.d = 0xc0;
                cpu.registers.e = 0xde;
            }
            0x46..=0x7e => {
                cpu.registers.h = 0xc0;
                cpu.registers.l = 0xde;
            }
            _ => assert!(false, "Unexpected op code {:02x}", op_code)
        }
        cpu.mmu.write_byte(0xc0de, 0x27);

        cpu.step();

        let loaded_value = match op_code {
            0x0a | 0x1a | 0x7e => cpu.registers.a,
            0x46 => cpu.registers.b,
            0x4e => cpu.registers.c,
            0x56 => cpu.registers.d,
            0x5e => cpu.registers.e,
            0x66 => cpu.registers.h,
            0x6e => cpu.registers.l,
            _ => panic!("Unexpected op code {:02x}", op_code)
        };

        assert_eq!(loaded_value, 0x27, "Op code failed to set register: {:02x}", op_code);
        assert_eq!(cpu.timer.accumulated_cycles, 8);
        assert_eq!(cpu.registers.f, flags_before);
    }
}

#[test]
fn ld_memory_at_u16_register_with_h_register() {
    let mut cpu = create_cpu(synthesize_cart!([0x74]));
    let flags_before = cpu.registers.f;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.step();
    let value = cpu.mmu.read_byte(0xc0de);
    assert_eq!(value, 0xc0);
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_memory_at_u16_register_with_l_register() {
    let mut cpu = create_cpu(synthesize_cart!([0x75]));
    let flags_before = cpu.registers.f;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.step();
    let value = cpu.mmu.read_byte(0xc0de);
    assert_eq!(value, 0xde);
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_u8_register_with_u8_register() {
    let op_codes = [
        0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4f,
        0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d, 0x5f,
        0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6f,
        0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7f
    ];

    for &op_code in op_codes.iter() {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        let flags_before = cpu.registers.f;
        match op_code & 0x0f { // drop op code table row digit so we can match on column
            0x00 | 0x08 => cpu.registers.b = 0x42,
            0x01 | 0x09 => cpu.registers.c = 0x42,
            0x02 | 0x0a => cpu.registers.d = 0x42,
            0x03 | 0x0b => cpu.registers.e = 0x42,
            0x04 | 0x0c => cpu.registers.h = 0x42,
            0x05 | 0x0d => cpu.registers.l = 0x42,
            0x07 | 0x0f => cpu.registers.a = 0x42,
            _ => assert!(false, "Unexpected op code {:02x}", op_code)
        }

        cpu.step();

        assert_eq!(cpu.timer.accumulated_cycles, 4, "Op code {:02x} failed", op_code);
        assert_eq!(cpu.registers.f, flags_before);

        match op_code {
            0x40..=0x47 => assert_eq!(cpu.registers.b, 0x42, "Op code {:02x} failed", op_code),
            0x48..=0x4f => assert_eq!(cpu.registers.c, 0x42, "Op code {:02x} failed", op_code),
            0x50..=0x57 => assert_eq!(cpu.registers.d, 0x42, "Op code {:02x} failed", op_code),
            0x58..=0x5f => assert_eq!(cpu.registers.e, 0x42, "Op code {:02x} failed", op_code),
            0x60..=0x67 => assert_eq!(cpu.registers.h, 0x42, "Op code {:02x} failed", op_code),
            0x68..=0x6f => assert_eq!(cpu.registers.l, 0x42, "Op code {:02x} failed", op_code),
            0x78..=0x7f => assert_eq!(cpu.registers.a, 0x42, "Op code {:02x} failed", op_code),
            _ => assert!(false, "Unexpected op code {:02x}", op_code)
        }
    }
}

#[test]
fn ld_hli_a() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.registers.a = 0x42;
    let cycles = cpu.ld_hli_a();
    assert_eq!(cycles, 4);
    assert_eq!(cpu.mmu.read_byte(0xc0de), 0x42);
    assert_eq!(cpu.registers.h, 0xc0);
    assert_eq!(cpu.registers.l, 0xdf);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_hli_a_instruction() {
    let mut cpu = create_cpu(synthesize_cart!([0x22]));
    let flags_before = cpu.registers.f;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.registers.a = 0x42;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.mmu.read_byte(0xc0de), 0x42);
    assert_eq!(cpu.registers.h, 0xc0);
    assert_eq!(cpu.registers.l, 0xdf);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_hld_a() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.registers.a = 0x42;
    let cycles = cpu.ld_hld_a();
    assert_eq!(cycles, 4);
    assert_eq!(cpu.mmu.read_byte(0xc0de), 0x42);
    assert_eq!(cpu.registers.h, 0xc0);
    assert_eq!(cpu.registers.l, 0xdd);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_hld_a_instruction() {
    let mut cpu = create_cpu(synthesize_cart!([0x32]));
    let flags_before = cpu.registers.f;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.registers.a = 0x42;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.mmu.read_byte(0xc0de), 0x42);
    assert_eq!(cpu.registers.h, 0xc0);
    assert_eq!(cpu.registers.l, 0xdd);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_a_hli() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.mmu.write_byte(0xc0de, 0x42);
    let cycles = cpu.ld_a_hli();
    assert_eq!(cycles, 4);
    assert_eq!(cpu.registers.a, 0x42);
    assert_eq!(cpu.registers.h, 0xc0);
    assert_eq!(cpu.registers.l, 0xdf);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_a_hli_instruction() {
    let mut cpu = create_cpu(synthesize_cart!([0x2a]));
    let flags_before = cpu.registers.f;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.mmu.write_byte(0xc0de, 0x42);
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0x42);
    assert_eq!(cpu.registers.h, 0xc0);
    assert_eq!(cpu.registers.l, 0xdf);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_a_hld() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.mmu.write_byte(0xc0de, 0x42);
    let cycles = cpu.ld_a_hld();
    assert_eq!(cycles, 4);
    assert_eq!(cpu.registers.a, 0x42);
    assert_eq!(cpu.registers.h, 0xc0);
    assert_eq!(cpu.registers.l, 0xdd);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_a_hld_instruction() {
    let mut cpu = create_cpu(synthesize_cart!([0x3a]));
    let flags_before = cpu.registers.f;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.mmu.write_byte(0xc0de, 0x42);
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0x42);
    assert_eq!(cpu.registers.h, 0xc0);
    assert_eq!(cpu.registers.l, 0xdd);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ldh_a8_a() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    cpu.registers.a = 0x13;
    let src = addressing::RegisterByte(U8Register::A);
    let immediate_byte = StaticByteAddressingMode(0x80, 4);
    let dst = addressing::MemoryAtU8Address(immediate_byte);
    let cycles = cpu.ld_u8(dst, src);
    assert_eq!(cycles, 8);
    assert_eq!(cpu.mmu.read_byte(0xff80), 0x13);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ldh_a8_a_instruction() {
    let mut cpu = create_cpu(synthesize_cart!([0xe0, 0x80]));
    let flags_before = cpu.registers.f;
    cpu.registers.a = 0x13;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.mmu.read_byte(0xff80), 0x13);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ldh_a_a8() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xff80, 0x13);
    let immediate_byte = StaticByteAddressingMode(0x80, 4);
    let src = addressing::MemoryAtU8Address(immediate_byte);
    let dst = addressing::RegisterByte(U8Register::A);
    let cycles = cpu.ld_u8(dst, src);
    assert_eq!(cycles, 8);
    assert_eq!(cpu.registers.a, 0x13);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ldh_a_a8_instruction() {
    let mut cpu = create_cpu(synthesize_cart!([0xf0, 0x80]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xff80, 0x13);
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.a, 0x13);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_memory_at_register_c_with_register_a() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    cpu.registers.a = 0x42;
    cpu.registers.c = 0x80;
    let dst = addressing::MemoryAtU8Address(addressing::RegisterByte(U8Register::C));
    let src = addressing::RegisterByte(U8Register::A);
    let cycles = cpu.ld_u8(dst, src);
    assert_eq!(cycles, 4);
    assert_eq!(cpu.mmu.read_byte(0xff80), 0x42);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_memory_at_register_c_with_register_a_instruction() {
    let mut cpu = create_cpu(synthesize_cart!([0xe2]));
    let flags_before = cpu.registers.f;
    cpu.registers.a = 0x42;
    cpu.registers.c = 0x80;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.mmu.read_byte(0xff80), 0x42);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_register_a_with_memory_at_c() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xff80, 0x42);
    cpu.registers.c = 0x80;
    let dst = addressing::RegisterByte(U8Register::A);
    let src = addressing::MemoryAtU8Address(addressing::RegisterByte(U8Register::C));
    let cycles = cpu.ld_u8(dst, src);
    assert_eq!(cycles, 4);
    assert_eq!(cpu.registers.a, 0x42);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_register_a_with_memory_at_c_instruction() {
    let mut cpu = create_cpu(synthesize_cart!([0xf2]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xff80, 0x42);
    cpu.registers.c = 0x80;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0x42);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_memory_at_immediate_a16_with_register_a() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    cpu.registers.a = 0x42;
    let immediate_byte = StaticWordAddressingMode(0xc0de, 8);
    let dst = addressing::MemoryAtU16Address(immediate_byte);
    let src = addressing::RegisterByte(U8Register::A);
    let cycles = cpu.ld_u8(dst, src);
    assert_eq!(cycles, 12);
    assert_eq!(cpu.mmu.read_byte(0xc0de), 0x42);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_memory_at_immediate_a16_with_register_a_instruction() {
    let mut cpu = create_cpu(synthesize_cart!([0xea, 0xde, 0xc0]));
    let flags_before = cpu.registers.f;
    cpu.registers.a = 0x42;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.mmu.read_byte(0xc0de), 0x42);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_register_a_with_memory_at_immediate_a16() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xc0de, 0x42);
    let immediate_byte = StaticWordAddressingMode(0xc0de, 8);
    let src = addressing::MemoryAtU16Address(immediate_byte);
    let dst = addressing::RegisterByte(U8Register::A);
    let cycles = cpu.ld_u8(dst, src);
    assert_eq!(cycles, 12);
    assert_eq!(cpu.registers.a, 0x42);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_register_a_with_memory_at_immediate_a16_instruction() {
    let mut cpu = create_cpu(synthesize_cart!([0xfa, 0xde, 0xc0]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xc0de, 0x42);
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.a, 0x42);
    assert_eq!(cpu.registers.f, flags_before);
}

// 8-bit arithmetic

#[test]
fn inc_r8_increments_an_8_bit_register() {
    let op_codes = [0x04, 0x14, 0x24, 0x0c, 0x1c, 0x2c, 0x3c];
    for &op_code in op_codes.iter() {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        let c_before = cpu.registers.flag_c();
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 4);
        match op_code {
            0x04 => assert_eq!(cpu.registers.b, 0x01),
            0x14 => assert_eq!(cpu.registers.d, 0x01),
            0x24 => assert_eq!(cpu.registers.h, 0x01),
            0x0c => assert_eq!(cpu.registers.c, 0x01),
            0x1c => assert_eq!(cpu.registers.e, 0x01),
            0x2c => assert_eq!(cpu.registers.l, 0x01),
            0x3c => assert_eq!(cpu.registers.a, 0x01),
            _ => panic!("Unexpected op code {:02x}", op_code)
        }
        assert!(!cpu.registers.flag_z());
        assert!(!cpu.registers.flag_n());
        assert!(!cpu.registers.flag_h());
        assert_eq!(cpu.registers.flag_c(), c_before);
    }
}

#[test]
fn inc_memory_at_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0x34]));
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    let c_before = cpu.registers.flag_c();
    assert_eq!(cpu.mmu.read_byte(0xc0de), 0);
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.mmu.read_byte(0xc0de), 1);
    assert!(!cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert_eq!(cpu.registers.flag_c(), c_before);
}

#[test]
fn inc_u8_flags() {
    // half-carry from bit 3 to bit 4
    let mut cpu = create_cpu(synthesize_cart!([0x04]));
    cpu.registers.b = 0b0000_1111;
    let c_before = cpu.registers.flag_c();
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.b, 0b0001_0000);
    assert!(!cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(cpu.registers.flag_h());
    assert_eq!(cpu.registers.flag_c(), c_before);

    // results is zero
    let mut cpu = create_cpu(synthesize_cart!([0x04]));
    cpu.registers.b = 0b1111_1111;
    let c_before = cpu.registers.flag_c();
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.b, 0b0000_0000);
    assert!(cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(cpu.registers.flag_h());
    assert_eq!(cpu.registers.flag_c(), c_before);
}

#[test]
fn dec_r8_decrements_an_8_bit_register() {
    let op_codes = [0x05, 0x15, 0x25, 0x0d, 0x1d, 0x2d, 0x3d];
    for &op_code in op_codes.iter() {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        cpu.registers.b = 0x43;
        cpu.registers.c = 0x43;
        cpu.registers.d = 0x43;
        cpu.registers.e = 0x43;
        cpu.registers.h = 0x43;
        cpu.registers.l = 0x43;
        cpu.registers.a = 0x43;
        let c_before = cpu.registers.flag_c();
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 4);
        match op_code {
            0x05 => assert_eq!(cpu.registers.b, 0x42),
            0x15 => assert_eq!(cpu.registers.d, 0x42),
            0x25 => assert_eq!(cpu.registers.h, 0x42),
            0x0d => assert_eq!(cpu.registers.c, 0x42),
            0x1d => assert_eq!(cpu.registers.e, 0x42),
            0x2d => assert_eq!(cpu.registers.l, 0x42),
            0x3d => assert_eq!(cpu.registers.a, 0x42),
            _ => panic!("Unexpected op code {:02x}", op_code)
        }
        assert!(!cpu.registers.flag_z());
        assert!(cpu.registers.flag_n());
        assert!(!cpu.registers.flag_h());
        assert_eq!(cpu.registers.flag_c(), c_before);
    }
}

#[test]
fn dec_u8_flags() {
    // half-carry reset when no borrow from 4th bit
    let mut cpu = create_cpu(synthesize_cart!([0x05]));
    cpu.registers.b = 0b0000_0010;
    let c_before = cpu.registers.flag_c();
    cpu.step();
    assert_eq!(cpu.registers.b, 0b0000_0001);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert_eq!(cpu.registers.flag_c(), c_before);

    // half-carry set when borrow from 4th bit
    let mut cpu = create_cpu(synthesize_cart!([0x05]));
    cpu.registers.b = 0b0001_0000;
    let c_before = cpu.registers.flag_c();
    cpu.step();
    assert_eq!(cpu.registers.b, 0b0000_1111);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(cpu.registers.flag_h());
    assert_eq!(cpu.registers.flag_c(), c_before);

    // result is zero
    let mut cpu = create_cpu(synthesize_cart!([0x05]));
    cpu.registers.b = 0b0000_0001;
    let c_before = cpu.registers.flag_c();
    cpu.step();
    assert_eq!(cpu.registers.b, 0b0000_0000);
    assert!(cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert_eq!(cpu.registers.flag_c(), c_before);
}

#[test]
fn dec_memory_at_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0x35]));
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.mmu.write_byte(0xc0de, 0x43);
    let c_before = cpu.registers.flag_c();
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.mmu.read_byte(0xc0de), 0x42);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert_eq!(cpu.registers.flag_c(), c_before);
}

#[test]
fn add_a_r8() {
    let op_codes = [0x80, 0x81, 0x82, 0x83, 0x84, 0x85];
    for &op_code in op_codes.iter() {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        match op_code {
            0x80 => cpu.registers.b = 0x42,
            0x81 => cpu.registers.c = 0x42,
            0x82 => cpu.registers.d = 0x42,
            0x83 => cpu.registers.e = 0x42,
            0x84 => cpu.registers.h = 0x42,
            0x85 => cpu.registers.l = 0x42,
            _ => panic!("Unexpected op code {:02x}", op_code)
        }
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 4);
        assert_eq!(cpu.registers.a, 0x42);
        assert_eq!(cpu.registers.flag_z(), false);
        assert_eq!(cpu.registers.flag_n(), false);
        assert_eq!(cpu.registers.flag_h(), false);
        assert_eq!(cpu.registers.flag_c(), false);
    }
}

#[test]
fn add_a_a() {
    let mut cpu = create_cpu(synthesize_cart!([0x87]));
    cpu.registers.a = 0x42;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0x84);
    assert_eq!(cpu.registers.flag_z(), false);
    assert_eq!(cpu.registers.flag_n(), false);
    assert_eq!(cpu.registers.flag_h(), false);
    assert_eq!(cpu.registers.flag_c(), false);
}

#[test]
fn add_a_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0x86]));
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.mmu.write_byte(0xc0de, 0x42);
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0x42);
    assert_eq!(cpu.registers.flag_z(), false);
    assert_eq!(cpu.registers.flag_n(), false);
    assert_eq!(cpu.registers.flag_h(), false);
    assert_eq!(cpu.registers.flag_c(), false);
}

#[test]
fn add_a_immediate_byte() {
    let mut cpu = create_cpu(synthesize_cart!([0xc6, 0x42]));
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0x42);
    assert_eq!(cpu.registers.flag_z(), false);
    assert_eq!(cpu.registers.flag_n(), false);
    assert_eq!(cpu.registers.flag_h(), false);
    assert_eq!(cpu.registers.flag_c(), false);
}

#[test]
fn add_a_flags() {
    // result is zero
    let mut cpu = create_cpu(synthesize_cart!([0x80]));
    cpu.registers.a = 0x80;
    cpu.registers.b = 0x80;
    cpu.step();
    assert_eq!(cpu.registers.a, 0x00);
    assert!(cpu.registers.flag_z());
    assert!(!cpu.registers.flag_h());
    assert!(cpu.registers.flag_c());

    // half-carry
    let mut cpu = create_cpu(synthesize_cart!([0x80]));
    cpu.registers.a = 0x08;
    cpu.registers.b = 0x08;
    cpu.step();
    assert_eq!(cpu.registers.a, 0x10);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());

    // carry
    let mut cpu = create_cpu(synthesize_cart!([0x80]));
    cpu.registers.a = 0x81;
    cpu.registers.b = 0x80;
    cpu.step();
    assert_eq!(cpu.registers.a, 0x01);
    assert!(!cpu.registers.flag_z());
    assert!(!cpu.registers.flag_h());
    assert!(cpu.registers.flag_c());
}

#[test]
fn sub_a() {
    let op_codes = [0x90, 0x91, 0x92, 0x93, 0x94, 0x95];
    for &op_code in op_codes.iter() {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        cpu.registers.a = 0x43;
        match op_code {
            0x90 => cpu.registers.b = 0x01,
            0x91 => cpu.registers.c = 0x01,
            0x92 => cpu.registers.d = 0x01,
            0x93 => cpu.registers.e = 0x01,
            0x94 => cpu.registers.h = 0x01,
            0x95 => cpu.registers.l = 0x01,
            _ => panic!("Unexpected op code {:02x}", op_code)
        }
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 4);
        assert_eq!(cpu.registers.a, 0x42);
        assert!(!cpu.registers.flag_z());
        assert!(cpu.registers.flag_n());
        assert!(!cpu.registers.flag_h());
        assert!(!cpu.registers.flag_c());
    }
}

#[test]
fn sub_a_a() {
    let mut cpu = create_cpu(synthesize_cart!([0x97]));
    cpu.registers.a = 0x42;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0x0);
    assert!(cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}


#[test]
fn sub_a_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0x96]));
    cpu.registers.a = 0x43;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.mmu.write_byte(0xc0de, 0x01);
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0x42);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn sub_a_immediate_byte () {
    let mut cpu = create_cpu(synthesize_cart!([0xd6, 0x01]));
    cpu.registers.a = 0x43;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0x42);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn sub_a_flags() {
    // result is zero
    let mut cpu = create_cpu(synthesize_cart!([0x90]));
    cpu.registers.a = 0x42;
    cpu.registers.b = 0x42;
    cpu.step();
    assert_eq!(cpu.registers.a, 0);
    assert!(cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());

    // H:  Set if there is a borrow from bit 4; otherwise reset
    let mut cpu = create_cpu(synthesize_cart!([0x90]));
    // borrow from bit 4
    cpu.registers.a = 0b0001_0000;
    cpu.registers.b = 0x01;
    cpu.step();
    assert_eq!(cpu.registers.a, 0b0000_1111);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(cpu.registers.flag_h()); // set
    assert!(!cpu.registers.flag_c());

    let mut cpu = create_cpu(synthesize_cart!([0x90]));
    // no borrow from bit 4
    cpu.registers.a = 0b0000_1000;
    cpu.registers.b = 0x01;
    cpu.step();
    assert_eq!(cpu.registers.a, 0b0000_0111);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h()); // reset
    assert!(!cpu.registers.flag_c());

    // CY:  Set to 1 when an operation results in carrying from or borrowing to bit 7.
    let mut cpu = create_cpu(synthesize_cart!([0x90]));
    // borrow (wrap)
    cpu.registers.a = 0b0000_0000;
    cpu.registers.b = 0x01;
    cpu.step();
    assert_eq!(cpu.registers.a, 0b1111_1111);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(cpu.registers.flag_h());
    assert!(cpu.registers.flag_c()); // set


    let mut cpu = create_cpu(synthesize_cart!([0x90]));
    // no borrow (wrap)
    cpu.registers.a = 0b0000_0011;
    cpu.registers.b = 0x01;
    cpu.step();
    assert_eq!(cpu.registers.a, 0b0000_0010);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c()); // reset
}

#[test]
fn and_a_r8() {
    let op_codes = [0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5];
    for &op_code in op_codes.iter() {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        cpu.registers.a = 0b1010_1010;
        match op_code {
            0xa0 => cpu.registers.b = 0b0101_1101,
            0xa1 => cpu.registers.c = 0b0101_1101,
            0xa2 => cpu.registers.d = 0b0101_1101,
            0xa3 => cpu.registers.e = 0b0101_1101,
            0xa4 => cpu.registers.h = 0b0101_1101,
            0xa5 => cpu.registers.l = 0b0101_1101,
            _ => panic!("Unexpected op code {:02x}", op_code)
        }
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 4);
        assert_eq!(cpu.registers.a, 0b0000_1000);
        assert!(!cpu.registers.flag_z());
        assert!(!cpu.registers.flag_n());
        assert!(cpu.registers.flag_h());
        assert!(!cpu.registers.flag_c());
    }
}
#[test]
fn and_a_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0xa6]));
    cpu.registers.a = 0b1010_1010;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.mmu.write_byte(0xc0de, 0b0101_1101);
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0b0000_1000);
    assert!(!cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn and_a_a() {
    let mut cpu = create_cpu(synthesize_cart!([0xa7]));
    cpu.registers.a = 0b1010_1010;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0b1010_1010);
    assert!(!cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn and_a_u8() {
    let mut cpu = create_cpu(synthesize_cart!([0xe6, 0b0101_1101]));
    cpu.registers.a = 0b1010_1010;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0b0000_1000);
    assert!(!cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn and_a_result_is_zero() {
    let mut cpu = create_cpu(synthesize_cart!([0xa0]));
    cpu.registers.a = 0b1010_1010;
    cpu.registers.b = 0b0101_0101;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0b0000_0000);
    assert!(cpu.registers.flag_z());
}

#[test]
fn or_a_r8() {
    let op_codes = [0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5];
    for &op_code in op_codes.iter() {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        cpu.registers.a = 0b1010_1010;
        match op_code {
            0xb0 => cpu.registers.b = 0b0101_0101,
            0xb1 => cpu.registers.c = 0b0101_0101,
            0xb2 => cpu.registers.d = 0b0101_0101,
            0xb3 => cpu.registers.e = 0b0101_0101,
            0xb4 => cpu.registers.h = 0b0101_0101,
            0xb5 => cpu.registers.l = 0b0101_0101,
            _ => panic!("Unexpected op code {:02x}", op_code)
        }
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 4);
        assert_eq!(cpu.registers.a, 0b1111_1111);
        assert!(!cpu.registers.flag_z());
        assert!(!cpu.registers.flag_n());
        assert!(!cpu.registers.flag_h());
        assert!(!cpu.registers.flag_c());
    }
}

#[test]
fn or_a_a() {
    let mut cpu = create_cpu(synthesize_cart!([0xb7]));
    cpu.registers.a = 0b1010_1010;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0b1010_1010);
    assert!(!cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn or_a_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0xb6]));
    cpu.registers.a = 0b1000_0000;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.mmu.write_byte(0xc0de, 0b0000_0001);
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0b1000_0001);
    assert!(!cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn or_a_immediate_byte() {
    let mut cpu = create_cpu(synthesize_cart!([0xf6, 0b0000_0001]));
    cpu.registers.a = 0b1000_0000;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0b1000_0001);
    assert!(!cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn or_a_result_is_zero() {
    let mut cpu = create_cpu(synthesize_cart!([0xb0]));
    cpu.registers.a = 0b0000_0000;
    cpu.registers.b = 0b0000_0000;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0b0000_0000);
    assert!(cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

// Extensively cover logic and flags
#[test]
fn adc_a() {
    for a in 0..=255 {
        for b in 0..=255 {
            for &f in [Registers::FLAG_NONE, Registers::FLAG_C].iter() {
                let c = if f == Registers::FLAG_C { 1 } else { 0 };
                let expected16: u16 = a + b + c;
                let expected8 = (a as u8).wrapping_add(b as u8).wrapping_add(c as u8);
                let expected4 = (a & 0b1111).wrapping_add(b & 0b1111).wrapping_add(c);

                let mut cpu = create_cpu(synthesize_cart!([0x88]));
                cpu.registers.a = a as u8;
                cpu.registers.b = b as u8;
                cpu.registers.f = f as u8;
                cpu.step();
                assert_eq!(cpu.timer.accumulated_cycles, 4);
                assert_eq!(cpu.registers.a, expected8);
                assert_eq!(cpu.registers.flag_z(), expected8 == 0);
                assert!(!cpu.registers.flag_n());
                assert_eq!(cpu.registers.flag_h(), expected4.bit(4));
                assert_eq!(cpu.registers.flag_c(), expected16.bit(8));
            }
        }
    }
}

#[test]
fn adc_a_r8() {
    for op_code in 0x88..=0x8d {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        cpu.registers.a = 0x01 as u8;
        match op_code {
            0x88 => cpu.registers.b = 0x01 as u8,
            0x89 => cpu.registers.c = 0x01 as u8,
            0x8a => cpu.registers.d = 0x01 as u8,
            0x8b => cpu.registers.e = 0x01 as u8,
            0x8c => cpu.registers.h = 0x01 as u8,
            0x8d => cpu.registers.l = 0x01 as u8,
            _ => panic!("Unexpected op code {:02x}", op_code)
        }
        cpu.registers.f = Registers::FLAG_C;
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 4);
        assert_eq!(cpu.registers.a, 0x03);
        assert!(!cpu.registers.flag_z());
        assert!(!cpu.registers.flag_n());
        assert!(!cpu.registers.flag_h());
        assert!(!cpu.registers.flag_c());
    }
}

#[test]
fn adc_a_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0x8e]));
    cpu.registers.a = 0xfd;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.registers.f = Registers::FLAG_C;
    cpu.mmu.write_byte(0xc0de, 0x01);
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0xff);
}

#[test]
fn adc_a_a() {
    let mut cpu = create_cpu(synthesize_cart!([0x8f]));
    cpu.registers.a = 0x01;
    cpu.registers.f = Registers::FLAG_C;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0x03);
}

#[test]
fn adc_a_immediate_byte() {
    let mut cpu = create_cpu(synthesize_cart!([0xce, 0x02]));
    cpu.registers.a = 0x01;
    cpu.registers.f = Registers::FLAG_C;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0x04);
}

#[test]
// Extensively cover logic and flags
fn sbc_a() {
    for a in 0..=255u16 {
        for b in 0..=255u16 {
            for f in [Registers::FLAG_NONE, Registers::FLAG_C] {
                let c = if f == Registers::FLAG_C { 1 } else { 0 };
                let expected16: u16 = a.wrapping_sub(b + c);
                let expected8 = (a as u8).wrapping_sub((b as u8).wrapping_add(c as u8));
                let expected4 = (a & 0b1111).wrapping_sub((b & 0b1111).wrapping_add(c));

                let mut cpu = create_cpu(synthesize_cart!([0x98]));
                cpu.registers.a = a as u8;
                cpu.registers.b = b as u8;
                cpu.registers.f = f as u8;
                cpu.step();
                assert_eq!(cpu.timer.accumulated_cycles, 4);
                assert_eq!(cpu.registers.a, expected8, "{} - ({} + {}) != {}", a, b, c, cpu.registers.a);
                assert_eq!(cpu.registers.flag_z(), expected8 == 0);
                assert!(cpu.registers.flag_n());
                assert_eq!(cpu.registers.flag_h(), expected4.bit(4));
                assert_eq!(cpu.registers.flag_c(), expected16.bit(8));
            }
        }
    }
}

#[test]
fn sbc_a_r8() {
    for op_code in 0x98..=0x9d {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        cpu.registers.a = 0x44;
        cpu.registers.f = Registers::FLAG_C;
        match op_code {
            0x98 => cpu.registers.b = 0x01 as u8,
            0x99 => cpu.registers.c = 0x01 as u8,
            0x9a => cpu.registers.d = 0x01 as u8,
            0x9b => cpu.registers.e = 0x01 as u8,
            0x9c => cpu.registers.h = 0x01 as u8,
            0x9d => cpu.registers.l = 0x01 as u8,
            _ => panic!("Unexpected op code {:02x}", op_code)
        }
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 4);
        assert_eq!(cpu.registers.a, 0x42);
        assert!(!cpu.registers.flag_z());
        assert!(cpu.registers.flag_n());
        assert!(!cpu.registers.flag_h());
        assert!(!cpu.registers.flag_c());
    }
}

#[test]
fn sbc_a_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0x9e]));
    cpu.registers.a = 0x44;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.mmu.write_byte(0xc0de, 0x01);
    cpu.registers.f = Registers::FLAG_C;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0x42);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn sbc_a_a() {
    let mut cpu = create_cpu(synthesize_cart!([0x9f]));
    cpu.registers.a = 0x44;
    cpu.registers.f = Registers::FLAG_C;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0xff);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(cpu.registers.flag_h());
    assert!(cpu.registers.flag_c());
}

#[test]
fn sbc_a_immediate() {
    let mut cpu = create_cpu(synthesize_cart!([0xde, 0x01]));
    cpu.registers.a = 0x44;
    cpu.registers.f = Registers::FLAG_C;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0x42);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn xor_a_r8() {
    for op_code in 0xa8..=0xad {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        cpu.registers.a = 0b1010_1010;
        match op_code {
            0xa8 => cpu.registers.b = 0b1100_0011,
            0xa9 => cpu.registers.c = 0b1100_0011,
            0xaa => cpu.registers.d = 0b1100_0011,
            0xab => cpu.registers.e = 0b1100_0011,
            0xac => cpu.registers.h = 0b1100_0011,
            0xad => cpu.registers.l = 0b1100_0011,
            _ => panic!("Unexpected op code {:02x}", op_code)
        }
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 4);
        assert_eq!(cpu.registers.a, 0b0110_1001);
        assert!(!cpu.registers.flag_z());
        assert!(!cpu.registers.flag_n());
        assert!(!cpu.registers.flag_h());
        assert!(!cpu.registers.flag_c());
    }
}

#[test]
fn xor_a_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0xae]));
    cpu.registers.a = 0b1010_1010;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.mmu.write_byte(0xc0de, 0b1100_0011);
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0b0110_1001);
    assert!(!cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn xor_a_a() {
    let mut cpu = create_cpu(synthesize_cart!([0xaf]));
    cpu.registers.a = 0b1010_1010;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0b0000_0000);
    assert!(cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn xor_a_immediate_byte() {
    let mut cpu = create_cpu(synthesize_cart!([0xee, 0b1100_0011]));
    cpu.registers.a = 0b1010_1010;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0b0110_1001);
    assert!(!cpu.registers.flag_z());
    assert!(!cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn cp_a_flags() {
    let mut cpu = create_cpu(synthesize_cart!([0xb8]));
    cpu.registers.a = 0x3c;
    cpu.registers.b = 0x2f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0x3c);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());

    let mut cpu = create_cpu(synthesize_cart!([0xb8]));
    cpu.registers.a = 0x3c;
    cpu.registers.b = 0x3c;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0x3c);
    assert!(cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());

    let mut cpu = create_cpu(synthesize_cart!([0xb8]));
    cpu.registers.a = 0x3c;
    cpu.registers.b = 0x40;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0x3c);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(cpu.registers.flag_c());
}

#[test]
fn cp_a_r8() {
    for op_code in 0xb8..=0xbd {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        cpu.registers.a = 0x3c;
        match op_code {
            0xb8 => cpu.registers.b = 0x40,
            0xb9 => cpu.registers.c = 0x40,
            0xba => cpu.registers.d = 0x40,
            0xbb => cpu.registers.e = 0x40,
            0xbc => cpu.registers.h = 0x40,
            0xbd => cpu.registers.l = 0x40,
            _ => panic!("Unexpected op code {:02x}", op_code)
        }
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 4);
        assert_eq!(cpu.registers.a, 0x3c);
        assert!(!cpu.registers.flag_z());
        assert!(cpu.registers.flag_n());
        assert!(!cpu.registers.flag_h());
        assert!(cpu.registers.flag_c());
    }
}

#[test]
fn cp_a_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0xbe]));
    cpu.registers.a = 0x3c;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.mmu.write_byte(0xc0de, 0x40);
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0x3c);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(cpu.registers.flag_c());
}

#[test]
fn cp_a_a() {
    let mut cpu = create_cpu(synthesize_cart!([0xbf]));
    cpu.registers.a = 0x3c;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.a, 0x3c);
    assert!(cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn cp_a_immediate_byte() {
    let mut cpu = create_cpu(synthesize_cart!([0xfe, 0x40]));
    cpu.registers.a = 0x3c;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.a, 0x3c);
    assert!(!cpu.registers.flag_z());
    assert!(cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(cpu.registers.flag_c());
}

#[test]
fn daa() {
    use std::ops::RangeInclusive;

    fn test_conditions(flags: u8, a_highs: RangeInclusive<u8>, a_lows: RangeInclusive<u8>, adjustment: u8, c_after: bool) {
        for high in a_highs {
            for low in a_lows.clone() {
                let a_before = (high << 4) | (low & 0x0f);
                let mut cpu = create_cpu(synthesize_cart!([0x27]));
                cpu.registers.f = flags;
                cpu.registers.a = a_before;
                let n_before = cpu.registers.flag_n();
                cpu.step();
                assert_eq!(cpu.registers.a, a_before.wrapping_add(adjustment));
                assert_eq!(cpu.registers.flag_z(), cpu.registers.a == 0);
                assert_eq!(cpu.registers.flag_n(), n_before);
                assert!(!cpu.registers.flag_h());
                assert_eq!(cpu.registers.flag_c(), c_after);
            }
        }
    }

    test_conditions(Registers::FLAG_NONE                                      , 0x0..=0x9 , 0x0..=0x9 , 0x00 , false);
    test_conditions(Registers::FLAG_NONE                                      , 0x0..=0x8 , 0xa..=0xf , 0x06 , false);
    test_conditions(Registers::FLAG_H                                         , 0x0..=0x9 , 0x0..=0x3 , 0x06 , false);
    test_conditions(Registers::FLAG_NONE                                      , 0xa..=0xf , 0x0..=0x9 , 0x60 , true);
    test_conditions(Registers::FLAG_NONE                                      , 0x9..=0xf , 0xa..=0xf , 0x66 , true);
    test_conditions(Registers::FLAG_H                                         , 0xa..=0xf , 0x0..=0x3 , 0x66 , true);
    test_conditions(Registers::FLAG_C                                         , 0x0..=0x2 , 0x0..=0x9 , 0x60 , true);
    test_conditions(Registers::FLAG_C                                         , 0x0..=0x2 , 0xa..=0xf , 0x66 , true);
    test_conditions(Registers::FLAG_C | Registers::FLAG_H                     , 0x0..=0x3 , 0x0..=0x3 , 0x66 , true);

    test_conditions(Registers::FLAG_N                                         , 0x0..=0x9 , 0x0..=0x9 , 0x00 , false);
    test_conditions(Registers::FLAG_N | Registers::FLAG_H                     , 0x0..=0x8 , 0x6..=0xf , 0xfa , false);
    test_conditions(Registers::FLAG_N | Registers::FLAG_C                     , 0x7..=0xf , 0x0..=0x9 , 0xa0 , true);
    test_conditions(Registers::FLAG_N | Registers::FLAG_H | Registers::FLAG_C , 0x6..=0xf , 0x6..=0xf , 0x9a , true);

}

#[test]
fn scf() {
    let all_flags_set = Registers::FLAG_Z | Registers::FLAG_N | Registers::FLAG_H | Registers::FLAG_C;
    for flags in Registers::FLAG_NONE..=all_flags_set {
        let mut cpu = create_cpu(synthesize_cart!([0x37]));
        cpu.registers.f = flags;
        let z_before = cpu.registers.flag_z();
        cpu.step();

        assert_eq!(cpu.timer.accumulated_cycles, 4);
        assert_eq!(cpu.registers.flag_z(), z_before);
        assert!(!cpu.registers.flag_n());
        assert!(!cpu.registers.flag_h());
        assert!(cpu.registers.flag_c());
    }
}

#[test]
fn cpl() {
    let all_flags_set = Registers::FLAG_Z | Registers::FLAG_N | Registers::FLAG_H | Registers::FLAG_C;
    for flags in Registers::FLAG_NONE..=all_flags_set {
        for a in 0x00..=0xff {
            let mut cpu = create_cpu(synthesize_cart!([0x2f]));
            cpu.registers.a = a;
            cpu.registers.f = flags;
            let z_before = cpu.registers.flag_z();
            let c_before = cpu.registers.flag_c();

            cpu.step();

            assert_eq!(cpu.timer.accumulated_cycles, 4);
            assert_eq!(cpu.registers.a, !a);
            assert_eq!(cpu.registers.flag_z(), z_before);
            assert!(cpu.registers.flag_n());
            assert!(cpu.registers.flag_h());
            assert_eq!(cpu.registers.flag_c(), c_before);
        }
    }
}

#[test]
fn ccf() {
    let all_flags_set = Registers::FLAG_Z | Registers::FLAG_N | Registers::FLAG_H | Registers::FLAG_C;
    for flags in Registers::FLAG_NONE..=all_flags_set {
        let mut cpu = create_cpu(synthesize_cart!([0x3f]));
        cpu.registers.f = flags;
        let z_before = cpu.registers.flag_z();
        let c_before = cpu.registers.flag_c();
        cpu.step();

        assert_eq!(cpu.timer.accumulated_cycles, 4);
        assert_eq!(cpu.registers.flag_z(), z_before);
        assert!(!cpu.registers.flag_n());
        assert!(!cpu.registers.flag_h());
        assert_eq!(cpu.registers.flag_c(), !c_before);
    }
}

#[test]
fn rlca() {
    let mut cpu = create_cpu(synthesize_cart!([0x07]));
    cpu.registers.a = 0b1100_0000;
    cpu.step();
    assert_eq!(cpu.registers.a, 0b1000_0001);
    assert!(cpu.registers.flag_c());

    // test all possible values of A
    for a in 0x00..=0xff {
        let mut cpu = create_cpu(synthesize_cart!([0x07]));
        cpu.registers.a = a;
        let expected = a.wrapping_shl(1) | a.wrapping_shr(7);
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 4);
        assert_eq!(cpu.registers.a, expected);
        assert!(!cpu.registers.flag_z());
        assert!(!cpu.registers.flag_n());
        assert!(!cpu.registers.flag_h());
        assert_eq!(cpu.registers.flag_c(), a.bit(7));
    }
}

#[test]
fn rla() {
    let mut cpu = create_cpu(synthesize_cart!([0x17]));
    cpu.registers.a = 0b0100_0000;
    cpu.registers.f = Registers::FLAG_C;
    cpu.step();
    assert_eq!(cpu.registers.a, 0b1000_0001, "{:08b} != {:08b}", cpu.registers.a, 0b1000_0001);
    assert!(!cpu.registers.flag_c());

    // test all possible values of A
    for flags in [Registers::FLAG_NONE, Registers::FLAG_C] {
        for a in 0x00..=0xff {
            let mut cpu = create_cpu(synthesize_cart!([0x17]));
            cpu.registers.a = a;
            cpu.registers.f = flags;
            let expected = a.wrapping_shl(1) | if flags == Registers::FLAG_C {
                0b0000_0001
            } else {
                0b0000_0000
            };
            cpu.step();
            assert_eq!(cpu.timer.accumulated_cycles, 4);
            assert_eq!(cpu.registers.a, expected);
            assert!(!cpu.registers.flag_z());
            assert!(!cpu.registers.flag_n());
            assert!(!cpu.registers.flag_h());
            assert_eq!(cpu.registers.flag_c(), a.bit(7));
        }
    }
}

#[test]
fn rrca() {
    let mut cpu = create_cpu(synthesize_cart!([0x0f]));
    cpu.registers.a = 0b0000_0011;
    cpu.step();
    assert_eq!(cpu.registers.a, 0b1000_0001);
    assert!(cpu.registers.flag_c());

    // test all possible values of A
    for a in 0x00..=0xff {
        let mut cpu = create_cpu(synthesize_cart!([0x0f]));
        cpu.registers.a = a;
        let expected = a.wrapping_shr(1) | a.wrapping_shl(7);
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 4);
        assert_eq!(cpu.registers.a, expected);
        assert!(!cpu.registers.flag_z());
        assert!(!cpu.registers.flag_n());
        assert!(!cpu.registers.flag_h());
        assert_eq!(cpu.registers.flag_c(), a.bit(0));
    }
}

#[test]
fn rra() {
    let mut cpu = create_cpu(synthesize_cart!([0x1f]));
    cpu.registers.a = 0b0000_0010;
    cpu.registers.f = Registers::FLAG_C;
    cpu.step();
    assert_eq!(cpu.registers.a, 0b1000_0001);
    assert!(!cpu.registers.flag_c());

    // test all possible values of A
    for flags in [Registers::FLAG_NONE, Registers::FLAG_C] {
        for a in 0x00..=0xff {
            let mut cpu = create_cpu(synthesize_cart!([0x1f]));
            cpu.registers.a = a;
            cpu.registers.f = flags;
            let expected = a.wrapping_shr(1) | if flags == Registers::FLAG_C {
                0b1000_0000
            } else {
                0b0000_0000
            };
            cpu.step();
            assert_eq!(cpu.timer.accumulated_cycles, 4);
            assert_eq!(cpu.registers.a, expected);
            assert!(!cpu.registers.flag_z());
            assert!(!cpu.registers.flag_n());
            assert!(!cpu.registers.flag_h());
            assert_eq!(cpu.registers.flag_c(), a.bit(0));
        }
    }
}

// 16-bit loads

#[test]
fn ld_u16() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    let src = StaticWordAddressingMode(0x1227, 8);
    let dst = addressing::RegisterWord(U16Register::BC);
    let cycles = cpu.ld_u16(dst, src);
    assert_eq!(cycles, 8);
    assert_eq!(cpu.registers.b, 0x12);
    assert_eq!(cpu.registers.c, 0x27);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn ld_u16_register_with_immediate_word() {
    let op_codes = [0x01, 0x11, 0x21, 0x31];
    for &op_code in op_codes.iter() {
        let mut cpu = create_cpu(synthesize_cart!([op_code, 0x27, 0x12]));
        let flags_before = cpu.registers.f;
        cpu.step();
        match op_code {
            0x01 => {
                assert_eq!(cpu.registers.b, 0x12);
                assert_eq!(cpu.registers.c, 0x27);
            }
            0x11 => {
                assert_eq!(cpu.registers.d, 0x12);
                assert_eq!(cpu.registers.e, 0x27);
            }
            0x21 => {
                assert_eq!(cpu.registers.h, 0x12);
                assert_eq!(cpu.registers.l, 0x27);
            }
            0x31 => {
                assert_eq!(cpu.registers.sp, 0x1227);
            }
            _ => assert!(false, "Unexpected op op code {:02x}", op_code)
        }
        assert_eq!(cpu.timer.accumulated_cycles, 12);
        assert_eq!(cpu.registers.f, flags_before);
    }
}

#[test]
fn ld_memory_at_16_with_sp() {
    let mut cpu = create_cpu(synthesize_cart!([0x08, 0xde, 0xc0]));
    let before_flags = cpu.registers.f;
    cpu.registers.sp = 0xbeef;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 20);
    assert_eq!(cpu.mmu.read_byte(0xc0de), 0xef);
    assert_eq!(cpu.mmu.read_byte(0xc0df), 0xbe);
    assert_eq!(cpu.registers.f, before_flags);
}

#[test]
fn ld_sp_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0xf9]));
    let before_flags = cpu.registers.f;
    cpu.registers.h = 0xbe;
    cpu.registers.l = 0xef;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.sp, 0xbeef);
    assert_eq!(cpu.registers.f, before_flags);
}

#[test]
fn ld_hl_sp_i8() {
    // zero immediate i8
    let mut cpu = create_cpu(synthesize_cart!([0xf8, 0x00]));
    cpu.registers.sp = 0xc0de;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.h, 0xc0); // upper
    assert_eq!(cpu.registers.l, 0xde); // lower

    // positive immediate i8
    let mut cpu = create_cpu(synthesize_cart!([0xf8, 0x01]));
    cpu.registers.sp = 0xc0de;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.h, 0xc0); // upper
    assert_eq!(cpu.registers.l, 0xdf); // lower

    // negative immediate i8
    let mut cpu = create_cpu(synthesize_cart!([0xf8, 0xff]));
    cpu.registers.sp = 0xc0de;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.h, 0xc0); // upper
    assert_eq!(cpu.registers.l, 0xdd); // lower
}

#[test]
fn ld_hl_sp_i8_underflow() {
    // underflow
    let mut cpu = create_cpu(synthesize_cart!([0xf8, 0x80]));
    cpu.registers.sp = 0x007f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.h, 0xff); // upper
    assert_eq!(cpu.registers.l, 0xff); // lower
}

#[test]
fn ld_hl_sp_i8_overflow() {
    // underflow
    let mut cpu = create_cpu(synthesize_cart!([0xf8, 0x7f]));
    cpu.registers.sp = 0xff81;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.h, 0x00); // upper
    assert_eq!(cpu.registers.l, 0x00); // lower
    assert_eq!(cpu.registers.flag_h(), true);
    assert_eq!(cpu.registers.flag_c(), true);
}

#[test]
fn ld_hl_sp_i8_flags() {
    //https://stackoverflow.com/questions/57958631/game-boy-half-carry-flag-and-16-bit-instructions-especially-opcode-0xe8#57978555
    //https://github.com/nbouchinet/gbmu/blob/master/src/cpu/Core.cpp#L40-L51
    let mut cpu = create_cpu(synthesize_cart!([0xf8, 0xff]));
    cpu.registers.sp = 0xffff;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.h, 0xff); // upper
    assert_eq!(cpu.registers.l, 0xfe); // lower
    assert_eq!(cpu.registers.flag_z(), false);
    assert_eq!(cpu.registers.flag_n(), false);
    assert_eq!(cpu.registers.flag_h(), true);
    assert_eq!(cpu.registers.flag_c(), true);

    let mut cpu = create_cpu(synthesize_cart!([0xf8, 0x08]));
    cpu.registers.sp = 0x08;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.h, 0x00); // upper
    assert_eq!(cpu.registers.l, 0x10); // lower
    assert_eq!(cpu.registers.flag_z(), false);
    assert_eq!(cpu.registers.flag_n(), false);
    assert_eq!(cpu.registers.flag_h(), true);
    assert_eq!(cpu.registers.flag_c(), false);

    let mut cpu = create_cpu(synthesize_cart!([0xf8, 0x08]));
    cpu.registers.sp = 0x00;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.h, 0x00); // upper
    assert_eq!(cpu.registers.l, 0x08); // lower
    assert_eq!(cpu.registers.flag_z(), false);
    assert_eq!(cpu.registers.flag_n(), false);
    assert_eq!(cpu.registers.flag_h(), false);
    assert_eq!(cpu.registers.flag_c(), false);
}

#[test]
fn push() {
    let op_codes = [0xc5, 0xd5, 0xe5, 0xf5];
    for &op_code in op_codes.iter() {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        match op_code {
            0xc5 => {
                cpu.registers.b = 0xc0;
                cpu.registers.c = 0xde;
            }
            0xd5 => {
                cpu.registers.d = 0xc0;
                cpu.registers.e = 0xde;
            }
            0xe5 => {
                cpu.registers.h = 0xc0;
                cpu.registers.l = 0xde;
            }
            0xf5 => {
                cpu.registers.a = 0xc0;
                cpu.registers.f = 0xde;
            }
            _ => assert!(false, "Unexpected op code: {:02x}", op_code)
        }
        let before_flags = cpu.registers.f;
        cpu.step();
        assert_eq!(cpu.registers.sp, 0xfffc);
        assert_eq!(cpu.mmu.read_byte(0xfffd), 0xc0);
        assert_eq!(cpu.mmu.read_byte(0xfffc), 0xde);
        assert_eq!(cpu.timer.accumulated_cycles, 16);
        assert_eq!(cpu.registers.f, before_flags);
    }
}

#[test]
fn pop() {
    let op_codes = [0xc1, 0xd1, 0xe1];
    for &op_code in op_codes.iter() {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        let before_flags = cpu.registers.f;
        cpu.mmu.write_byte(0xfffd, 0xbe);
        cpu.mmu.write_byte(0xfffc, 0xef);
        cpu.registers.sp = 0xfffc;
        cpu.step();
        assert_eq!(cpu.registers.sp, 0xfffe);
        match op_code {
            0xc1 => {
                assert_eq!(cpu.registers.b, 0xbe);
                assert_eq!(cpu.registers.c, 0xef);
            }
            0xd1 => {
                assert_eq!(cpu.registers.d, 0xbe);
                assert_eq!(cpu.registers.e, 0xef);
            }
            0xe1 => {
                assert_eq!(cpu.registers.h, 0xbe);
                assert_eq!(cpu.registers.l, 0xef);
            },
            _ => assert!(false, "Unexpected op code: {:02x}", op_code)
        }
        assert_eq!(cpu.registers.f, before_flags);
        assert_eq!(cpu.timer.accumulated_cycles, 12);
    }
}

#[test]
fn pop_af() {
    let mut cpu = create_cpu(synthesize_cart!([0xf1]));
    cpu.mmu.write_byte(0xfffd, 0xbe);
    cpu.mmu.write_byte(0xfffc, 0xef);
    cpu.registers.sp = 0xfffc;
    cpu.step();
    assert_eq!(cpu.registers.a, 0xbe);
    assert_eq!(cpu.registers.f, 0xef);
    assert_eq!(cpu.registers.sp, 0xfffe);
    assert_eq!(cpu.timer.accumulated_cycles, 12);
}

// 16-bit arithmetic

#[test]
fn add_sp_i8() {
    // zero immediate i8
    let mut cpu = create_cpu(synthesize_cart!([0xe8, 0x00]));
    cpu.registers.sp = 0xc0de;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.sp, 0xc0de);

    // positive immediate i8
    let mut cpu = create_cpu(synthesize_cart!([0xe8, 0x01]));
    cpu.registers.sp = 0xc0de;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.sp, 0xc0df);

    // negative immediate i8
    let mut cpu = create_cpu(synthesize_cart!([0xe8, 0xff]));
    cpu.registers.sp = 0xc0de;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.sp, 0xc0dd);
}

#[test]
fn add_sp_i8_underflow() {
    // underflow
    let mut cpu = create_cpu(synthesize_cart!([0xe8, 0x80]));
    cpu.registers.sp = 0x007f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.sp, 0xffff);
}

#[test]
fn add_sp_i8_overflow() {
    // underflow
    let mut cpu = create_cpu(synthesize_cart!([0xe8, 0x7f]));
    cpu.registers.sp = 0xff81;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.sp, 0x0000);
    assert_eq!(cpu.registers.flag_h(), true);
    assert_eq!(cpu.registers.flag_c(), true);
}

#[test]
fn add_sp_i8_flags() {
    //https://stackoverflow.com/questions/57958631/game-boy-half-carry-flag-and-16-bit-instructions-especially-opcode-0xe8#57978555
    //https://github.com/nbouchinet/gbmu/blob/master/src/cpu/Core.cpp#L40-L51
    let mut cpu = create_cpu(synthesize_cart!([0xe8, 0xff]));
    cpu.registers.sp = 0xffff;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.sp, 0xfffe);
    assert_eq!(cpu.registers.flag_z(), false);
    assert_eq!(cpu.registers.flag_n(), false);
    assert_eq!(cpu.registers.flag_h(), true);
    assert_eq!(cpu.registers.flag_c(), true);

    let mut cpu = create_cpu(synthesize_cart!([0xe8, 0x08]));
    cpu.registers.sp = 0x08;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.sp, 0x0010);
    assert_eq!(cpu.registers.flag_z(), false);
    assert_eq!(cpu.registers.flag_n(), false);
    assert_eq!(cpu.registers.flag_h(), true);
    assert_eq!(cpu.registers.flag_c(), false);

    let mut cpu = create_cpu(synthesize_cart!([0xe8, 0x08]));
    cpu.registers.sp = 0x00;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.sp, 0x0008);
    assert_eq!(cpu.registers.flag_z(), false);
    assert_eq!(cpu.registers.flag_n(), false);
    assert_eq!(cpu.registers.flag_h(), false);
    assert_eq!(cpu.registers.flag_c(), false);
}

#[test]
fn inc_r16() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    cpu.registers.b = 0x12;
    cpu.registers.c = 0x27;
    let flags_before = cpu.registers.f;

    let cycles = cpu.inc_r16(addressing::RegisterWord(U16Register::BC));
    assert_eq!(cycles, 4);
    assert_eq!(cpu.registers.b, 0x12);
    assert_eq!(cpu.registers.c, 0x28);
    // Flags are not modified
    assert_eq!(cpu.registers.f, flags_before);

    // Overflowing the lower register in the pair results in incrementing the upper register
    cpu.registers.b = 0x00;
    cpu.registers.c = 0xff;
    let cycles = cpu.inc_r16(addressing::RegisterWord(U16Register::BC));
    assert_eq!(cycles, 4);
    assert_eq!(cpu.registers.b, 0x01);
    assert_eq!(cpu.registers.c, 0x00);

    // Overflow wraps around. This is an assumption right now because I can't find any
    // documentation that directly asserts this, but it appears that most emulators take
    // advantage of C-like wrapping behavior in whatever language they use.

    cpu.registers.b = 0xff;
    cpu.registers.c = 0xff;

    let cycles = cpu.inc_r16(addressing::RegisterWord(U16Register::BC));
    assert_eq!(cycles, 4);
    assert_eq!(cpu.registers.b, 0x00);
    assert_eq!(cpu.registers.c, 0x00);
}

#[test]
fn inc_u16_instructions() {
    let mut cpu = create_cpu(synthesize_cart!([0x03, 0x13, 0x23, 0x33]));
    let flags_before = cpu.registers.f;
    cpu.registers.b = 0x00;
    cpu.registers.c = 0xff;
    cpu.registers.d = 0x00;
    cpu.registers.e = 0xff;
    cpu.registers.h = 0x00;
    cpu.registers.l = 0xff;
    cpu.registers.sp = 0x00ff;
    for _ in 0..4 {
        cpu.step();
    }
    assert_eq!(cpu.registers.b, 0x01);
    assert_eq!(cpu.registers.c, 0x00);
    assert_eq!(cpu.registers.d, 0x01);
    assert_eq!(cpu.registers.e, 0x00);
    assert_eq!(cpu.registers.h, 0x01);
    assert_eq!(cpu.registers.l, 0x00);
    assert_eq!(cpu.registers.sp, 0x0100);
    assert_eq!(cpu.timer.accumulated_cycles, 32);
    // Flags are not modified
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn dec_r16() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    cpu.registers.b = 0x12;
    cpu.registers.c = 0x27;
    let flags_before = cpu.registers.f;

    let cycles = cpu.dec_r16(addressing::RegisterWord(U16Register::BC));
    assert_eq!(cycles, 4);
    assert_eq!(cpu.registers.b, 0x12);
    assert_eq!(cpu.registers.c, 0x26);
    // Flags are not modified
    assert_eq!(cpu.registers.f, flags_before);

    // Underflowing the lower register in the pair results in decrementing the upper register
    cpu.registers.b = 0x01;
    cpu.registers.c = 0x00;
    let cycles = cpu.dec_r16(addressing::RegisterWord(U16Register::BC));
    assert_eq!(cycles, 4);
    assert_eq!(cpu.registers.b, 0x00);
    assert_eq!(cpu.registers.c, 0xff);

    // Overflow wraps around. This is an assumption right now because I can't find any
    // documentation that directly asserts this, but it appears that most emulators take
    // advantage of C-like wrapping behavior in whatever language they use.

    cpu.registers.b = 0x00;
    cpu.registers.c = 0x00;

    let cycles = cpu.dec_r16(addressing::RegisterWord(U16Register::BC));
    assert_eq!(cycles, 4);
    assert_eq!(cpu.registers.b, 0xff);
    assert_eq!(cpu.registers.c, 0xff);
}

#[test]
fn dec_u16_instructions() {
    let mut cpu = create_cpu(synthesize_cart!([0x0b, 0x1b, 0x2b, 0x3b]));
    let flags_before = cpu.registers.f;
    cpu.registers.b = 0x01;
    cpu.registers.c = 0x00;
    cpu.registers.d = 0x01;
    cpu.registers.e = 0x00;
    cpu.registers.h = 0x01;
    cpu.registers.l = 0x00;
    cpu.registers.sp = 0x0100;
    for _ in 0..4 {
        cpu.step();
    }
    assert_eq!(cpu.registers.b, 0x00);
    assert_eq!(cpu.registers.c, 0xff);
    assert_eq!(cpu.registers.d, 0x00);
    assert_eq!(cpu.registers.e, 0xff);
    assert_eq!(cpu.registers.h, 0x00);
    assert_eq!(cpu.registers.l, 0xff);
    assert_eq!(cpu.registers.sp, 0x00ff);
    assert_eq!(cpu.timer.accumulated_cycles, 32);
    // Flags are not modified
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn add_hl_r16() {
    let op_codes = [0x09, 0x19, 0x39];
    for &op_code in op_codes.iter() {
        let mut cpu = create_cpu(synthesize_cart!([op_code]));
        cpu.registers.h = 0x01;
        cpu.registers.l = 0x01;
        match op_code {
            0x09 => {
                cpu.registers.b = 0xbe;
                cpu.registers.c = 0xef;
            }
            0x19 => {
                cpu.registers.d = 0xbe;
                cpu.registers.e = 0xef;
            }
            0x39 => {
                cpu.registers.sp = 0xbeef;
            }
            _ => assert!(false, "Unexpected op code: {:02x}", op_code)
        }
        let original_z = cpu.registers.flag_z();
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 8);
        assert_eq!(cpu.registers.h, 0xbf);
        assert_eq!(cpu.registers.l, 0xf0);
        assert_eq!(cpu.registers.flag_z(), original_z);
        assert!(!cpu.registers.flag_n());
    }
}

#[test]
fn add_hl_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0x29]));
    cpu.registers.h = 0x01;
    cpu.registers.l = 0x01;
    let original_z = cpu.registers.flag_z();
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.h, 0x02);
    assert_eq!(cpu.registers.l, 0x02);
    assert_eq!(cpu.registers.flag_z(), original_z);
    assert!(!cpu.registers.flag_n());
}

#[test]
fn add_hl_r16_half_carry() {
    let mut cpu = create_cpu(synthesize_cart!([0x09]));
    cpu.registers.h = 0x0f;
    cpu.registers.l = 0xff;
    cpu.registers.b = 0x00;
    cpu.registers.c = 0x01;
    let original_z = cpu.registers.flag_z();
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.h, 0x10);
    assert_eq!(cpu.registers.l, 0x00);
    assert_eq!(cpu.registers.flag_z(), original_z);
    assert!(!cpu.registers.flag_n());
    assert!(cpu.registers.flag_h());
    assert!(!cpu.registers.flag_c());
}

#[test]
fn add_hl_r16_full_carry() {
    let mut cpu = create_cpu(synthesize_cart!([0x09]));
    cpu.registers.h = 0xf0;
    cpu.registers.l = 0x01;
    cpu.registers.b = 0x10;
    cpu.registers.c = 0x00;
    let original_z = cpu.registers.flag_z();
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.h, 0x00);
    assert_eq!(cpu.registers.l, 0x01);
    assert_eq!(cpu.registers.flag_z(), original_z);
    assert!(!cpu.registers.flag_n());
    assert!(!cpu.registers.flag_h());
    assert!(cpu.registers.flag_c());
}

// Jumps

#[test]
fn jr() {
    // zero immediate
    let mut cpu = create_cpu(synthesize_cart!([0x18, 0x00]));
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.pc, 0x0102);
    assert_eq!(cpu.registers.f, flags_before);

    // +1 immediate
    let mut cpu = create_cpu(synthesize_cart!([0x18, 0x01]));
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.pc, 0x0103);

    // -1 immediate
    let mut cpu = create_cpu(synthesize_cart!([0x18, 0xff]));
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.pc, 0x0101);
}

#[test]
fn jr_nz() {
    let mut cpu = create_cpu(synthesize_cart!([0x20, 0x01]));
    cpu.registers.f = Registers::FLAG_NONE;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.pc, 0x0103);

    let mut cpu = create_cpu(synthesize_cart!([0x20, 0x01]));
    cpu.registers.f = Registers::FLAG_Z;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.pc, 0x0102);
}

#[test]
fn jr_nc() {
    let mut cpu = create_cpu(synthesize_cart!([0x30, 0x01]));
    cpu.registers.f = Registers::FLAG_NONE;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.pc, 0x0103);

    let mut cpu = create_cpu(synthesize_cart!([0x30, 0x01]));
    cpu.registers.f = Registers::FLAG_C;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.pc, 0x0102);
}

#[test]
fn jr_z() {
    let mut cpu = create_cpu(synthesize_cart!([0x28, 0x01]));
    cpu.registers.f = Registers::FLAG_Z;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.pc, 0x0103);

    let mut cpu = create_cpu(synthesize_cart!([0x28, 0x01]));
    cpu.registers.f = Registers::FLAG_NONE;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.pc, 0x0102);
}

#[test]
fn jr_c() {
    let mut cpu = create_cpu(synthesize_cart!([0x38, 0x01]));
    cpu.registers.f = Registers::FLAG_C;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.pc, 0x0103);

    let mut cpu = create_cpu(synthesize_cart!([0x38, 0x01]));
    cpu.registers.f = Registers::FLAG_NONE;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.pc, 0x0102);
}

#[test]
fn jp() {
    let mut cpu = create_cpu(synthesize_cart!([]));
    let flags_before = cpu.registers.f;
    let am = StaticWordAddressingMode(0x1227, 8);
    let cycles = cpu.jp(Condition::Unconditional, am);
    assert_eq!(cycles, 12);
    assert_eq!(cpu.registers.pc, 0x1227);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn jp_immediate() {
    let mut cpu = create_cpu(synthesize_cart!([0xc3, 0x27, 0x12]));
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.pc, 0x1227);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn jp_hl() {
    let mut cpu = create_cpu(synthesize_cart!([0xe9]));
    let flags_before = cpu.registers.f;
    cpu.registers.h = 0xc0;
    cpu.registers.l = 0xde;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.pc, 0xc0de);
}

#[test]
fn jp_nz() {
    let mut cpu = create_cpu(synthesize_cart!([0xc2, 0x27, 0x12]));
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.pc, 0x1227);
    assert_eq!(cpu.registers.f, flags_before);

    let mut cpu = create_cpu(synthesize_cart!([0xc2, 0x27, 0x12]));
    cpu.registers.f |= Registers::FLAG_Z;
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.pc, 0x0103);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn jp_nc() {
    let mut cpu = create_cpu(synthesize_cart!([0xd2, 0x27, 0x12]));
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.pc, 0x1227);
    assert_eq!(cpu.registers.f, flags_before);

    let mut cpu = create_cpu(synthesize_cart!([0xd2, 0x27, 0x12]));
    cpu.registers.f |= Registers::FLAG_C;
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.pc, 0x0103);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn jp_z() {
    let mut cpu = create_cpu(synthesize_cart!([0xca, 0x27, 0x12]));
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.pc, 0x0103);
    assert_eq!(cpu.registers.f, flags_before);

    let mut cpu = create_cpu(synthesize_cart!([0xca, 0x27, 0x12]));
    cpu.registers.f |= Registers::FLAG_Z;
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.pc, 0x1227);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn jp_c() {
    let mut cpu = create_cpu(synthesize_cart!([0xda, 0x27, 0x12]));
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.registers.pc, 0x0103);
    assert_eq!(cpu.registers.f, flags_before);

    let mut cpu = create_cpu(synthesize_cart!([0xda, 0x27, 0x12]));
    cpu.registers.f |= Registers::FLAG_C;
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.pc, 0x1227);
    assert_eq!(cpu.registers.f, flags_before);
}

#[test]
fn rst() {
    for op_code in [0xc7, 0xcf, 0xd7, 0xdf, 0xe7, 0xef, 0xf7, 0xff] {
        let mut cpu = create_cpu(synthesize_cart!([op_code])); // RST 00
        let flags_before = cpu.registers.f;
        cpu.step();
        assert_eq!(cpu.timer.accumulated_cycles, 16);
        assert_eq!(cpu.mmu.read_byte(0xfffd), 0x01);
        assert_eq!(cpu.mmu.read_byte(0xfffc), 0x01);
        assert_eq!(cpu.registers.f, flags_before);
        match op_code {
            0xc7 => assert_eq!(cpu.registers.pc, 0x0000),
            0xcf => assert_eq!(cpu.registers.pc, 0x0008),
            0xd7 => assert_eq!(cpu.registers.pc, 0x0010),
            0xdf => assert_eq!(cpu.registers.pc, 0x0018),
            0xe7 => assert_eq!(cpu.registers.pc, 0x0020),
            0xef => assert_eq!(cpu.registers.pc, 0x0028),
            0xf7 => assert_eq!(cpu.registers.pc, 0x0030),
            0xff => assert_eq!(cpu.registers.pc, 0x0038),
            _ => panic!("Unexpected op code {:02x}", op_code)
        }
    }
}

#[test]
fn call_nz() {
    // z is set
    let mut cpu = create_cpu(synthesize_cart!([0xc4, 0xde, 0xc0]));
    cpu.registers.f |= Registers::FLAG_Z;
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.mmu.read_byte(0xfffd), 0x00); // PC upper
    assert_eq!(cpu.mmu.read_byte(0xfffc), 0x00); // PC lower
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.pc, 0x0103);

    // z is reset
    let mut cpu = create_cpu(synthesize_cart!([0xc4, 0xde, 0xc0]));
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 24);
    assert_eq!(cpu.mmu.read_byte(0xfffd), 0x01); // PC upper
    assert_eq!(cpu.mmu.read_byte(0xfffc), 0x03); // PC lower
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.pc, 0xc0de);
}

#[test]
fn call_nc() {
    // c is set
    let mut cpu = create_cpu(synthesize_cart!([0xd4, 0xde, 0xc0]));
    cpu.registers.f |= Registers::FLAG_C;
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.mmu.read_byte(0xfffd), 0x00); // PC upper
    assert_eq!(cpu.mmu.read_byte(0xfffc), 0x00); // PC lower
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.pc, 0x0103);

    // c is reset
    let mut cpu = create_cpu(synthesize_cart!([0xd4, 0xde, 0xc0]));
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 24);
    assert_eq!(cpu.mmu.read_byte(0xfffd), 0x01); // PC upper
    assert_eq!(cpu.mmu.read_byte(0xfffc), 0x03); // PC lower
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.pc, 0xc0de);
}

#[test]
fn call_z() {
    // z is set
    let mut cpu = create_cpu(synthesize_cart!([0xcc, 0xde, 0xc0]));
    cpu.registers.f |= Registers::FLAG_Z;
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 24);
    assert_eq!(cpu.mmu.read_byte(0xfffd), 0x01); // PC upper
    assert_eq!(cpu.mmu.read_byte(0xfffc), 0x03); // PC lower
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.pc, 0xc0de);

    // z is reset
    let mut cpu = create_cpu(synthesize_cart!([0xcc, 0xde, 0xc0]));
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.mmu.read_byte(0xfffd), 0x00); // PC upper
    assert_eq!(cpu.mmu.read_byte(0xfffc), 0x00); // PC lower
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.pc, 0x0103);
}

#[test]
fn call_c() {
    // c is set
    let mut cpu = create_cpu(synthesize_cart!([0xdc, 0xde, 0xc0]));
    cpu.registers.f |= Registers::FLAG_C;
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 24);
    assert_eq!(cpu.mmu.read_byte(0xfffd), 0x01); // PC upper
    assert_eq!(cpu.mmu.read_byte(0xfffc), 0x03); // PC lower
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.pc, 0xc0de);

    // c is reset
    let mut cpu = create_cpu(synthesize_cart!([0xdc, 0xde, 0xc0]));
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 12);
    assert_eq!(cpu.mmu.read_byte(0xfffd), 0x00); // PC upper
    assert_eq!(cpu.mmu.read_byte(0xfffc), 0x00); // PC lower
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.pc, 0x0103);
}

#[test]
fn call() {
    let mut cpu = create_cpu(synthesize_cart!([0xcd, 0xde, 0xc0]));
    let flags_before = cpu.registers.f;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 24);
    assert_eq!(cpu.mmu.read_byte(0xfffd), 0x01); // PC upper
    assert_eq!(cpu.mmu.read_byte(0xfffc), 0x03); // PC lower
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.pc, 0xc0de);
}

#[test]
fn ret_nz() {
    // z is set
    let mut cpu = create_cpu(synthesize_cart!([0xc0]));
    cpu.registers.f |= Registers::FLAG_Z;
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xfffd, 0xc0);
    cpu.mmu.write_byte(0xfffc, 0xde);
    cpu.registers.sp = 0xfffc;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.sp, 0xfffc);
    assert_eq!(cpu.registers.pc, 0x0101);

    // z is reset
    let mut cpu = create_cpu(synthesize_cart!([0xc0]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xfffd, 0xc0);
    cpu.mmu.write_byte(0xfffc, 0xde);
    cpu.registers.sp = 0xfffc;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 20);
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.sp, 0xfffe);
    assert_eq!(cpu.registers.pc, 0xc0de);
}

#[test]
fn ret_nc() {
    // c is set
    let mut cpu = create_cpu(synthesize_cart!([0xd0]));
    cpu.registers.f |= Registers::FLAG_C;
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xfffd, 0xc0);
    cpu.mmu.write_byte(0xfffc, 0xde);
    cpu.registers.sp = 0xfffc;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.sp, 0xfffc);
    assert_eq!(cpu.registers.pc, 0x0101);

    // c is reset
    let mut cpu = create_cpu(synthesize_cart!([0xd0]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xfffd, 0xc0);
    cpu.mmu.write_byte(0xfffc, 0xde);
    cpu.registers.sp = 0xfffc;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 20);
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.sp, 0xfffe);
    assert_eq!(cpu.registers.pc, 0xc0de);
}

#[test]
fn ret_z() {
    // z is set
    let mut cpu = create_cpu(synthesize_cart!([0xc8]));
    cpu.registers.f |= Registers::FLAG_Z;
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xfffd, 0xc0);
    cpu.mmu.write_byte(0xfffc, 0xde);
    cpu.registers.sp = 0xfffc;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 20);
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.sp, 0xfffe);
    assert_eq!(cpu.registers.pc, 0xc0de);

    // z is reset
    let mut cpu = create_cpu(synthesize_cart!([0xc8]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xfffd, 0xc0);
    cpu.mmu.write_byte(0xfffc, 0xde);
    cpu.registers.sp = 0xfffc;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.sp, 0xfffc);
    assert_eq!(cpu.registers.pc, 0x0101);
}

#[test]
fn ret_c() {
    // c is set
    let mut cpu = create_cpu(synthesize_cart!([0xd8]));
    cpu.registers.f |= Registers::FLAG_C;
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xfffd, 0xc0);
    cpu.mmu.write_byte(0xfffc, 0xde);
    cpu.registers.sp = 0xfffc;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 20);
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.sp, 0xfffe);
    assert_eq!(cpu.registers.pc, 0xc0de);

    // c is reset
    let mut cpu = create_cpu(synthesize_cart!([0xd8]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xfffd, 0xc0);
    cpu.mmu.write_byte(0xfffc, 0xde);
    cpu.registers.sp = 0xfffc;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 8);
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.sp, 0xfffc);
    assert_eq!(cpu.registers.pc, 0x0101);
}

#[test]
fn ret() {
    let mut cpu = create_cpu(synthesize_cart!([0xc9]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xfffd, 0xc0);
    cpu.mmu.write_byte(0xfffc, 0xde);
    cpu.registers.sp = 0xfffc;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.sp, 0xfffe);
    assert_eq!(cpu.registers.pc, 0xc0de);
}

#[test]
fn reti() {
    let mut cpu = create_cpu(synthesize_cart!([0xd9]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xfffd, 0xc0);
    cpu.mmu.write_byte(0xfffc, 0xde);
    cpu.registers.sp = 0xfffc;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 16);
    assert_eq!(cpu.registers.f, flags_before);
    assert_eq!(cpu.registers.sp, 0xfffe);
    assert_eq!(cpu.registers.pc, 0xc0de);
    assert!(cpu.mmu.interrupts.ime);
}

#[test]
fn reti_invalidates_pending_di() {
    let mut cpu = create_cpu(synthesize_cart!([0xf3, 0xd9]));
    let flags_before = cpu.registers.f;
    cpu.mmu.write_byte(0xfffd, 0xc0);
    cpu.mmu.write_byte(0xfffc, 0xde);
    cpu.registers.sp = 0xfffc;
    cpu.step();
    cpu.step();
    assert_eq!(cpu.registers.pc, 0xc0de);
    assert!(cpu.mmu.interrupts.ime);
}

// Misc

#[test]
fn di() {
    let mut cpu = create_cpu(synthesize_cart!([0xf3, 0x00]));
    cpu.mmu.interrupts.ime = true;
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert!(cpu.mmu.interrupts.ime);
    cpu.step();
    assert!(!cpu.mmu.interrupts.ime);
}

#[test]
fn ei() {
    let mut cpu = create_cpu(synthesize_cart!([0xfb, 0x00]));
    cpu.step();
    assert_eq!(cpu.timer.accumulated_cycles, 4);
    assert!(!cpu.mmu.interrupts.ime);
    cpu.step();
    assert!(cpu.mmu.interrupts.ime);
}

// Internals

#[test]
fn take_byte() {
    let mut cpu = create_cpu(synthesize_cart!([0x00, 0xc3, 0x27, 0x12]));
    assert_eq!(cpu.take_byte(), 0x00);
    assert_eq!(cpu.take_byte(), 0xc3);
}

#[test]
fn take_word() {
    let mut cpu = create_cpu(synthesize_cart!([0x27, 0x12]));
    assert_eq!(cpu.take_word(), 0x1227);
}
