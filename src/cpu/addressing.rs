use crate::cpu::{CPU, U8Register, U16Register};
use crate::mmu::{ReadByte, WriteByte};
use crate::utils::{JoinBytes, SplitBytes};

pub trait ReadByteAddressingMode {
  fn load(&self, cpu: &mut CPU) -> (u8, u8);
}

pub trait ReadSignedByteAddressingMode {
    fn load(&self, cpu: &mut CPU) -> (u8, i8);
}

pub trait WriteByteAddressingMode {
  fn store(&self, cpu: &mut CPU, value: u8) -> u8;
}

pub trait ReadWordAddressingMode {
  fn load(&self, cpu: &mut CPU) -> (u8, u16);
}

pub trait WriteWordAddressingMode {
  fn store(&self, cpu: &mut CPU, value: u16) -> u8 ;
}

pub struct ImmediateByte;

impl ReadByteAddressingMode for ImmediateByte {
  fn load(&self, cpu: &mut CPU) -> (u8, u8) {
      (4, cpu.take_byte())
  }
}

pub struct ImmediateSignedByte;

impl ReadSignedByteAddressingMode for ImmediateSignedByte {
    fn load(&self, cpu: &mut CPU) -> (u8, i8) {
        let (load_i8_cycles, immediate) = ImmediateByte.load(cpu);
        (load_i8_cycles, immediate as i8)
    }
}

pub struct ImmediateWord;

impl ReadWordAddressingMode for ImmediateWord {
  fn load(&self, cpu: &mut CPU) -> (u8, u16) {
      (8, cpu.take_word())
  }
}

pub struct RegisterByte(pub U8Register);

impl ReadByteAddressingMode for RegisterByte {
  fn load(&self, cpu: &mut CPU) -> (u8, u8) {
      let value = match self.0 {
          U8Register::A => cpu.registers.a,
          U8Register::B => cpu.registers.b,
          U8Register::C => cpu.registers.c,
          U8Register::D => cpu.registers.d,
          U8Register::E => cpu.registers.e,
          U8Register::H => cpu.registers.h,
          U8Register::L => cpu.registers.l,
          U8Register::F => cpu.registers.f
      };
      (0, value)
  }
}

impl WriteByteAddressingMode for RegisterByte {
  fn store(&self, cpu: &mut CPU, value: u8) -> u8 {
      match self.0 {
          U8Register::A => cpu.registers.a = value,
          U8Register::B => cpu.registers.b = value,
          U8Register::C => cpu.registers.c = value,
          U8Register::D => cpu.registers.d = value,
          U8Register::E => cpu.registers.e = value,
          U8Register::H => cpu.registers.h = value,
          U8Register::L => cpu.registers.l = value,
          U8Register::F => cpu.registers.f = value
      }
      0
  }
}

#[derive(Clone, Copy)]
pub struct RegisterWord(pub U16Register);

impl ReadWordAddressingMode for RegisterWord {
    fn load(&self, cpu: &mut CPU) -> (u8, u16) {
        let value = match self.0 {
            U16Register::AF => (cpu.registers.a, cpu.registers.f).join(),
            U16Register::BC => (cpu.registers.b, cpu.registers.c).join(),
            U16Register::DE => (cpu.registers.d, cpu.registers.e).join(),
            U16Register::HL => (cpu.registers.h, cpu.registers.l).join(),
            U16Register::SP => cpu.registers.sp,
            U16Register::PC => cpu.registers.pc
        };
        (0, value)
    }
}

impl WriteWordAddressingMode for RegisterWord {
    fn store(&self, cpu: &mut CPU, value: u16) -> u8 {
        match self.0 {
            U16Register::AF => {
                let (upper, lower) = value.split();
                cpu.registers.a = upper;
                cpu.registers.f = lower;
            }
            U16Register::BC => {
                let (upper, lower) = value.split();
                cpu.registers.b = upper;
                cpu.registers.c = lower;
            }
            U16Register::DE => {
                let (upper, lower) = value.split();
                cpu.registers.d = upper;
                cpu.registers.e = lower;
            }
            U16Register::HL => {
                let (upper, lower) = value.split();
                cpu.registers.h = upper;
                cpu.registers.l = lower;
            }
            U16Register::SP => {
                cpu.registers.sp = value;
            }
            U16Register::PC => {
                cpu.registers.pc = value;
            }
        }
        0
    }
}

pub struct MemoryAtU16Address<AM: ReadWordAddressingMode>(pub AM);

impl <AM: ReadWordAddressingMode>ReadByteAddressingMode for MemoryAtU16Address<AM> {
  fn load(&self, cpu: &mut CPU) -> (u8, u8) {
      let (load_address_cycles, address) = self.0.load(cpu);
      let value = cpu.mmu.read_byte(address);
      (load_address_cycles + 4, value)
  }
}

impl <AM: ReadWordAddressingMode>WriteByteAddressingMode for MemoryAtU16Address<AM> {
    fn store(&self, cpu: &mut CPU, value: u8) -> u8 {
      let (load_address_cycles, address) = self.0.load(cpu);
      cpu.mmu.write_byte(address, value);
      load_address_cycles + 4
    }
}

impl <AM: ReadWordAddressingMode>WriteWordAddressingMode for MemoryAtU16Address<AM> {
    fn store(&self, cpu: &mut CPU, value: u16) -> u8 {
        let (load_address_cycles, address) = self.0.load(cpu);
        let (upper, lower) = value.split();
        cpu.mmu.write_byte(address, lower);
        cpu.mmu.write_byte(address + 1, upper);
        load_address_cycles + 8
    }
}

pub struct MemoryAtU8Address<AM: ReadByteAddressingMode>(pub AM);

impl <AM: ReadByteAddressingMode>ReadByteAddressingMode for MemoryAtU8Address<AM> {
  fn load(&self, cpu: &mut CPU) -> (u8, u8) {
      let (load_address_cycles, a8_address) = self.0.load(cpu);
      let address = 0xff00 + a8_address as u16;
      let value = cpu.mmu.read_byte(address);
      (load_address_cycles + 4, value)
  }
}

impl <AM: ReadByteAddressingMode>WriteByteAddressingMode for MemoryAtU8Address<AM> {
    fn store(&self, cpu: &mut CPU, value: u8) -> u8 {
      let (load_address_cycles, a8_address) = self.0.load(cpu);
      let address = 0xff00 + a8_address as u16;
      cpu.mmu.write_byte(address, value);
      load_address_cycles + 4
    }
}

#[cfg(test)]
mod test {
    use crate::cpu::addressing::{ImmediateSignedByte, ReadSignedByteAddressingMode};
    use crate::cpu::test::{create_cpu};
    use crate::synthesize_cart;

    #[test]
    fn read_immediate_signed_byte_test() {
        let mut cpu = create_cpu(synthesize_cart!([0x00, 0x01, 0xff]));
        let am = ImmediateSignedByte;
        assert_eq!(am.load(&mut cpu), (4, 0));
        assert_eq!(am.load(&mut cpu), (4, 1));
        assert_eq!(am.load(&mut cpu), (4, -1));

    }
}
